<?php

/**
 * 
 * search.php
 * 
 * Searches for videos.
 * Displays videos that have a title or description 
 * that match the sentence the user introduces

 * 
 * HTML/CSS/JS Hentet fra http://bootsnipp.com/snippets/featured/login-and-register-tabbed-form
 * Deretter modifisert for vårt bruk
 *
 */
session_start();					// Start the session
require_once 'include/db.php'; // Connect to the database
require_once 'classes/user.inc.php'; // Get user class
require_once 'classes/video.inc.php'; // Get video class

$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) { // request was sent with ajax, show only content.
	$ajax = true;
}

$user = new User($db); // Create user object.
$video = new Video($db); // Create user object

$pageTitle = 'Video DB - Search'; // Finn på no :-P
if(!$ajax) // No header on ajax requests.
  require_once 'include/header.php';

$results = array();
if(isset($_GET['search']))
  $results = $video->find($_GET['search']);

if(count($results)==0) {
  $msg = array(
    'title' => "Obs!",
    'text' => "No videos found!."
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

if(!$ajax) {
?>
<div id="main-content">
<?php
}

?>


<div class="container">
  <h2>Search results <small><?php echo htmlspecialchars($_GET['search']); ?></small></h2>
  <ul class="list-unstyled">
<?php
foreach($results as $result) {
?>
		<li class="media">
			<div class="media">
				<div class="media-left media-middle">
					<a href="player.php?id=<?php echo $result->id; ?>">
						<img class="media-object thumbnail" src="<?php echo $result->getThumb(); ?>"  alt="Sample Image">
					</a>
				</div>
			<div class="media-body">
			
			<h4 class="media-heading"> <a href="player.php?id=<?php echo $result->id; ?>"> <?php  echo $result->title; ?>  </h4>
									   <?php  echo $result->description; ?>
				</div>
			</div>
		</li>
<?php 
}
?>
  </ul>
</div>

<?php
require_once 'include/footer.php';