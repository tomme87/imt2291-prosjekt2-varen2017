<?php


/**
 * upload.php:
 *
 * Shows a form to upload videos.
 * The user must enter the title and description and
 * select the file to be uploaded
 *
 * See also:
 * Upload.js
 *
 *
 */
session_start();          // Start the session
require_once 'include/db.php'; // Connect to the database
require_once 'classes/user.inc.php'; // Get user class

$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) { // request was sent with ajax, show only content.
	$ajax = true;
}

$user = new User($db); // Create user object.

$pageTitle = 'Video DB - New video'; // Finn på no :-P
//$cssInc = array("fileinput.min.css"); // Ekstra .css filer å inkludere. ****Hvorfor??
if(!$ajax) // No header on ajax requests.
	require_once 'include/header.php';

if(!$user->isLoggedIn()) {
  $msg = array(
    'title' => "Sorry!",
    'text' => "You have to be logged in to upload videos!."
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}



if(!$ajax) {
?>
<div id="main-content">
<?php
}
if($ajax && isset($cssInc)) {
  foreach($cssInc as $c) {
    echo "<link rel=\"stylesheet\" href=\"css/$c\" />\n";
  }
}
?>



<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 col-xs-12">
      <h1>Upload Video</h2>
      <form id="upload-form" action="receiveFile.php" method="post" role="form">
        <div class="form-group">
          <label for="title" class="control-label">Tittel</label>
          <input type="text" name="title" class="form-control" id="title" value="<?php if(isset($_POST['title'])) echo htmlspecialchars($_POST['title']); ?>">
        </div>
        <div class="form-group">
          <label for="description" class="control-label">Beskrivelse</label>
          <textarea name="description" class="form-control" rows="5" id="description" value="<?php if(isset($_POST['description'])) echo htmlspecialchars($_POST['description']); ?>" > </textarea>
        </div>
        <div class="form-group">
          <label for="input-id" class="control-label">Select File</label>
          <div id="errorBlock" class="help-block file-error-message" style="display: none;"></div>
          <input id="input-id" name="file" type="file" class="file">
        </div>
      </form>
      <div class="progress">
        <div id="progress" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">
          0%
        </div>
      </div>
      <div id="uploadSuccess" class="alert alert-success" style="display: none;" role="alert"><strong>Congratulations!</strong> File uploaded!
	  
			<div class="from group">
				<h1>Do you want to upload subtitles?</h1>
				<a id="ulLink" class="btn btn-primary btn-lg" type="button" href="#" role="button">Upload</a>
			</div>	
		
		</div>	  
      <div id="uploadFail" class="alert alert-danger" style="display: none;" role="alert"><strong>Sorry!</strong> <span id="failtext"></span></div>
	  
    </div>
  </div>
 </div> 

<?php

$jsIncInline = array('
  initUpload();
');
//$jsInc = array("fileinput.min.js", "upload.js"); // Ekstra .js filer å inkludere.
require_once 'include/footer.php';