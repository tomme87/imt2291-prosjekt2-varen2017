/**
 * Adds functionallity to login page to be able to switch betweeb register and login tab.
 *
 * http://bootsnipp.com/snippets/featured/login-and-register-tabbed-form
 */
// Moved to function, so it can be called after ajax request to login.php
function initFormActions() {
  /* Vis Login form */
  $('#login-form-link').click(function(e) {
    $("#login-form").delay(100).fadeIn(100);
    $("#register-form").fadeOut(100);
    $('#register-form-link').removeClass('active');
    $(this).addClass('active');
    e.preventDefault();
	});
  /* Vis Register form */
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
  
  /* Header må oppdateres med JS om denne skal aktiveres. Samt kanskje gjøre noe annet enn å sende til index.php etter login.
  $(document).on('submit', 'form[data-pjax]', function(event) {
    $.pjax.submit(event, '#main-content');
  });
  */
}
