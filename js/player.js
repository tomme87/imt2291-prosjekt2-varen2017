/**
 * Everything regarding the video player.
 */

/**
 * Sleep for a while.
 */
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Used to show picture attached to the video.
 * This adds the class talkingHead to the videoHolder DIV. This moves the video down to the corner.
 * Add the img to the img element and show it (fadeIn).
 *
 * @param img string with path to the image.
 *
 */
function showImage(img) {
  $('#videoHolder').addClass('talkingHead');
  $('.largeImage').attr('src', img).fadeIn('slow');
}

/**
 * This a reverse of the previos function. moves the video back and hide the image.
 */
function hideImage() {
  $('#videoHolder').removeClass('talkingHead');
  $('.largeImage').fadeOut('slow', function() {
    $(this).removeAttr('src');
  });
}


/**
 * Function to initialize Video-player with videojs functions.
 */
function initPlayer() {

  
  if(videojs.getPlayers()['my-player']) { // "uninitialize" player if already exists. 
      delete videojs.getPlayers()['my-player'];
  }
  
  var tlength = $('.subtitle').length;
  var ilength = $('.imgsub').length;
  var player = videojs('my-player',{
	/*textTrackSettings: false,*/
	
    playbackRates: [0.5, 1, 1.5, 2], // Ability to play video at differende speeds.
    //nativeTextTracks: false
  }).ready(function() {
    if(tlength) { // We got trac elements. (subtitles)
      // Set up any options.
      var options = {
        showTitle: false,
        showTrackSelector: true,
      };
    
      // Initialize the plugin.
      var transcript = this.transcript(options);
    
      // Then attach the widget to the page.
      var transcriptContainer = document.querySelector('#transcript');
      transcriptContainer.appendChild(transcript.el()); 
    } else { // No subs.
      $('#transcript').html('<p>This video does not have a transcript.</p>');
    }
    
    //console.log(ilength);
    // Source: https://github.com/videojs/video.js/blob/master/docs/guides/text-tracks.md
    if(ilength) { // We have "subtitle" with images.
      var tracks = player.textTracks();
      var metadataTrack;

      for (var i = 0; i < tracks.length; i++) {
        var track = tracks[i];

        // Find the metadata track that's labeled "ads".
        if (track.kind === 'metadata' && track.label === 'images') {
          track.mode = 'hidden';

          // Store it for usage outside of the loop.
          metadataTrack = track;
        }
      }
      //console.log(metadataTrack);
      
      // When the cue changes. do something.
      metadataTrack.addEventListener('cuechange', function() {
        //console.log(this.activeCues.length);
        if(this.activeCues.length) { // Entering
          showImage(this.activeCues[0].text); // Show image
        } else { // Leaving
          hideImage(); // Hide image.
        }
      });

    }
    
    /*this.on('progress', function(e){
      console.log( this.remainingTime() );
    });*/
  });
  
  $('.thumbnail.current').parents('.item').addClass('active'); // Set current (playlist) group as active.
  
  /**
   * Function to get time played 
   */
	function getPlayedTime() {
		
		// console.log("hello");
		 var current = player.currentTime();
		 var duration = player.duration();
		// console.log(current);
		//console.log(player.duration());
		
	     var totalPlayed = 0;	      
              	
        	
        if((player.currentTime()/player.duration())*100>80){
        	//console.log((player.currentTime()/player.duration())*100);
        	return 1;        		
        }else return 0; 	
        
	}
   
	
	var registered=0;
  
	$("#my-player").click(function() {
		
		//console.log($(this).attr('data-videoID'));
		//console.log($(this).attr('data-userID'));
		
		if(getPlayedTime()==1&&registered==0){
		
      $.post("getPlays.php", {videoID: $("#my-player").attr('data-videoID'), userID: $("#my-player").attr('data-userID') } );
	    registered =1;
		}
		
	});	
	
	player.on('ended', function() {
		if(getPlayedTime()==1&&registered==0){
			
			$.post("getPlays.php", {videoID: $("#my-player").attr('data-videoID'), userID: $("#my-player").attr('data-userID') } );
			registered =1;
		}
	});

  // When editVideo form is submitted
  $('#editVideo').submit(function(e) {
    e.preventDefault();
    
    $.post("takeEdit.php?do=editVideo", $(this).serialize(), function(data) { // Do the ajax call
      //console.log(data);
      if(data.ok != null) {
        $('#info-title').text(data.video.title); // Update title
        $('#vidDescription').text(data.video.description); // Update description
        $('#myModal2').modal('hide'); // Hide the modal.
      }
    }, "json");
  });
  
  // When addVideo link is pressed
  $('.addVideo').click(function(e) {
    e.preventDefault();
    var href = $(this).attr('href');
    $.get(href, function(data) { // Do ajax call
      $.pjax({url: data.url, container: '#main-content'}); //Make pjax go the given URL
    }, 'json');
  });
  
  // When new playlist form is submitted
  $('#newPlaylist').submit(function(e) {
    e.preventDefault();
    $.post($(this).attr('action'), $(this).serialize(), function(data) { // Do the request with ajax.
      $('#myModal').modal('hide').on('hidden.bs.modal', function (e) { // Hide the modal, and when done ..
        $.pjax({url: data.url, container: '#main-content'}); // Make pjax go to given URL.
      });
    }, 'json');
  });
	
}
