/**
 * editPlaylist.js
 * 
 * Treats changes in playlists 
 */


function initEditPlaylist() {
  $('.sortable').sortable();
  
  $('#saveButton').on('click', function () { // Save new playlist
    var $btn = $(this).button('loading'); // Change state of button
    
    var videos = [];
    $('.video-item').each(function(index) { // For each list item.
      var vid = $(this).data('vid'); // Video ID
      var id = $(this).data('id'); // Playlist ID
      var oldPos = $(this).data('position'); // Old position
      var newPos = index+1;
      videos.push({vid: vid, oldPos: oldPos, newPos: newPos});
    });
   // console.log(videos);
    
    $.post("takeEdit.php?do=editPlaylist&id="+$(this).data('id'), { videos: videos }, function(data) {
      //console.log(data);
      $('#alertbox').show(); // show alertbox
      $btn.button('reset');
    });
    
    
  });
  
  $('.delvid').on('click', function () { // Delte from playlist.
    var row = $(this).parent();
    var $btn = $(this).button('loading'); // Change state of button
    var vid = $(this).parent().data('vid'); // Video ID
    var id = $(this).data('id'); // Playlist ID
    $.post("takeEdit.php?do=delFromPlaylist&id="+id, { vid: vid }, function(data) {
     // console.log(data);
      $(row).remove();
    });
  });
}