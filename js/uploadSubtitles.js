/**
 * Handles upload subtitles page.
 */
function initUploadSubtitles() {

  $("#input-id").fileinput({
    showPreview: false,
    allowedFileExtensions: ["vtt"],
    elErrorContainer: "#errorBlock"
  });

  $("#upload-form").submit(function(e) {
    e.preventDefault();
    files = $("#input-id").prop("files");
    var formData = new FormData($(this)[0]);
    //console.log(files);
    if (files.length==1) {    // Tillat kun en og en files
      var file = files[0];  // Litt kortere enn å referere til elementet i arrayen hver gang vil trenger informasjon om filen.
      var xhr = new XMLHttpRequest();   // Overfør filen via AJAX
      xhr.open("POST", 'receiveSubtitle.php', true);    // Hvilket skript skal ta i mot filen
      xhr.setRequestHeader("X_FILENAME", file.name);  // Overfør informasjon om filnavnet
      xhr.setRequestHeader("X_MIMETYPE", file.type);  // Overfør informasjon om mime type
      xhr.setRequestHeader("X_FILESIZE", file.size);  // Overfør informasjon om filstørrelse
      xhr.upload.onprogress = function (event) {    // Motta beskjed underveis i overføringen
        if (event.lengthComputable) {   // Har overføringen startet
          // Her kan en eventuelt også oppdatere med antall bytes overført/totalt antall bytes som skal overføres/estimert tid som gjennstår
          var complete = (event.loaded/event.total*100|0);
          //console.log(complete);
          $('#progress').text(complete).attr("aria-valuenow", complete).css('width', complete+'%'); // Oppdater progressbar
        }
      }
      xhr.onload = function(e) {    // Kalles når overføringer er ferdig
        if (this.status == 200) { // Dersom alt er OK
          if(this.response) { // Got response
            try { // Incase we don't get valid JSON.
              data = jQuery.parseJSON(this.response);
              if(typeof data.error !== 'undefined') { // Error. !isset(data.error)
                $('#failtext').text(data.description);
                $('#uploadFail').show(); // Vis beskjed om feil
                $('#uploadSuccess').hide(); // Hide success alert
              } else { // Success
                $("#upload-form")[0].reset(); // Reset form
                $('#uploadFail').hide(); // Hide failed upload alert
              //  $('#ulLink').prop("href", "uploadSubtitles.php?id="+data.video.id+"&title="+encodeURIComponent(data.video.title)); // Set URL for upload subs.
                $('#uploadSuccess').show(); // Show success alert
              }
            } catch(err) {
              $('#failtext').text("Det oppsto en ukjent feil.");
              $('#uploadFail').show(); // Show fail alert.
              $('#uploadSuccess').hide(); // Hide success alert
            }
          }
        }
      }
      xhr.send(formData);    // Send filen
    }
  });

}