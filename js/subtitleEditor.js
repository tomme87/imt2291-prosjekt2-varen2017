/**
 * This script handles everything with the subtitle editor.
 */

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Used to format the time from seconds to hh:mm:ss.mmmm
 *
 * @param int seconds
 * @return string 
 */
function formatTime(t) {
  var tms = t * 1000;
  var ms = parseInt(tms%1000);
  var s = parseInt((tms/1000)%60);
  var m = parseInt((tms/(1000*60))%60);
  var h = parseInt((tms/(1000*60*60))%24);
  
  h = (h < 10) ? "0" + h : h;
  m = (m < 10) ? "0" + m : m;
  s = (s < 10) ? "0" + s : s;
  ms = (ms < 10) ? "00" + ms : ms;
  ms = (ms < 100 && ms >= 10) ? "0" + ms : ms;
  
  return h + ':' + m + ':' + s + '.' + ms
}

/**
 * This function (re)initializes the transcript-sidebar on the page.
 */ 
function refreshTranscript(player) {
  // Set up any options.
  var options = {
    showTitle: false,
    showTrackSelector: true,
  };

  // Initialize the plugin.
  var transcript = player.transcript(options);

  // Then attach the widget to the page.
  var transcriptContainer = document.querySelector('#transcript');
  transcriptContainer.innerHTML = ''; // Remove existing content.
  transcriptContainer.appendChild(transcript.el()); 
}


/**
 * Create VTT and save to DB
 */ 
async function saveTrack(track, id) {
  await sleep(2);
  
  // Create the .vtt file
  var vtt = "WEBVTT\n\n";
//  console.log(track);
  if(track.cues.length) { 
    $.each(track.cues, function(index, value) { // For each cue in track
     // console.log(index, value);
      vtt += (index+1) + "\n" + formatTime(value.startTime) + ' --> ' + formatTime(value.endTime) + "\n" + value.text + "\n\n";
    });
  }
  //console.log(vtt);
  
  var videoID = $("#my-player").data('videoid');
  
  $.post("takeEdit.php?do=saveSub", {vtt: vtt, id: id, vid: videoID }, function(data) {
   // console.log(data);
  });
}

/**
 * Initalize function to subtitle editor
 */
function initSubtitleEditor() {
  
  var player = videojs('my-player'); // Get VideoJS player
  player.ready(function () {
    var textTrack = this.textTracks()[0]; // Get first text track.
    console.log(textTrack);
    //saveTrack(textTrack);
    
    textTrack.mode = 'hidden'; // Don't show caption overlay
    textTrack.addEventListener('cuechange', function() { // One cue change.
      var cue = this.activeCues[0]; // Get the active cue (assumeing only one).
      if(cue == null) return 0; // Make sure we got a new cue.
      // Update input boxes.
      $('#cueText').val(cue.text);
      $('#startTime').val(cue.startTime);
      $('#endTime').val(cue.endTime);
    });
    
    refreshTranscript(player); // init transcript sidebar
    
    
    
  });
  
  player.markers({markers: []}); // initialize markers
  
  player.on('loadedmetadata', function() { // All metadata is loaded, textTrack is filled with cues.
    console.log("hei");
    var textTrack = this.textTracks()[0]; // Get first text track.
    
    // Add markers here.      
    var markers = []; 
    $.each(textTrack.cues, function(index, cue) { // For each cue in track
      // Legg til {time: xx, text: "tekst"} objecter i markers array.
      markers.push({time: cue.startTime, text: cue.text});
    });
    //console.log(markers);
    player.markers.add(markers); // Add markers.
  });
  
  // unfortunately this removes cue from active cue list :(
  /*$('.inputs').focus(function() { // Pause player when editing
    player.pause();
  });*/
  
  /*
  $('.inputs').blur(function() {
    console.log("keks");
    player.play();
  });
  */
  
  //Remove subtitles   
  	
 	$('#remove').click(function(){
    // if(!values.endTime || !values.startTime || !values.cueText) return;
 	  //console.log("hallo");
    var track = player.textTracks()[0];
 	  track.removeCue(track.activeCues[0]);
 	  saveTrack(track, $('#saveForm input[name=current]').val());
    refreshTranscript(player); 
     
 	  return;	
 	});
  
  $('#saveForm').submit(function(e) {
    e.preventDefault();
    
    //console.log(player.currentTime());
    
    // Get form values.
    var values = {};
    $.each($(this).serializeArray(), function(i, field) {
      values[field.name] = field.value;
    });
    
    //console.log(values);
    if(!values.endTime || !values.startTime || !values.cueText) return;
    
    var track = player.textTracks()[0]; // Get track element.
    
    if(track.activeCues.length && !values.new) { // Remove old cue if new.
      track.removeCue(track.activeCues[0]);
    } else if(!values.new && !track.activeCues.length) {
      console.log("Error, could not find cue to replace.");
      return;
    }
    track.addCue(new VTTCue(values.startTime, values.endTime, values.cueText)); // Add cue.
    
    // Because firefox does not sort this properly!!
    if(track.cues.cues_ != null) {
      track.cues.cues_.sort(function(a, b){
        if (a.startTime === undefined || a.startTime < b.startTime) {
          return -1;
        }
        if (b.startTime === undefined || b.startTime < a.startTime) {
          return 1;
        }
        return 0;
      });
    }
    
    refreshTranscript(player); // Refresh the transcript sidebar.
    //console.log(track);
    
    saveTrack(track, values.current); // Create and save track to DB.
  });
  
  $("#input-id").fileinput({
    showPreview: false,
    //allowedFileExtensions: ["mp4", "webm", "ogg"],
    elErrorContainer: "#errorBlock"
  });
  
  $("#upload-form").submit(function(e) {
    e.preventDefault();
    
    // Update input-fields.
    $('#upload-form input[name=startTime]').val(player.currentTime());
    $('#upload-form input[name=endTime]').val(player.currentTime() + parseInt($('#upload-form input[name=duration]').val()));
    
    files = $("#input-id").prop("files");
    var formData = new FormData($(this)[0]);
    //console.log(files);
    if (files.length==1) {    // Tillat kun en og en files
      var file = files[0];  // Litt kortere enn å referere til elementet i arrayen hver gang vil trenger informasjon om filen.
      var xhr = new XMLHttpRequest();   // Overfør filen via AJAX
      xhr.open("POST", 'receivePicture.php', true);    // Hvilket skript skal ta i mot filen
      xhr.setRequestHeader("X_FILENAME", file.name);  // Overfør informasjon om filnavnet
      xhr.setRequestHeader("X_MIMETYPE", file.type);  // Overfør informasjon om mime type
      xhr.setRequestHeader("X_FILESIZE", file.size);  // Overfør informasjon om filstørrelse
      xhr.upload.onprogress = function (event) {    // Motta beskjed underveis i overføringen
        if (event.lengthComputable) {   // Har overføringen startet
          // Her kan en eventuelt også oppdatere med antall bytes overført/totalt antall bytes som skal overføres/estimert tid som gjennstår
          var complete = (event.loaded/event.total*100|0);
          //console.log(complete);
          $('#progress').text(complete).attr("aria-valuenow", complete).css('width', complete+'%'); // Oppdater progressbar
        }
      }
      xhr.onload = function(e) {    // Kalles når overføringer er ferdig
        if (this.status == 200) { // Dersom alt er OK
          if(this.response) { // Got response
            try { // Incase we don't get valid JSON.
              data = jQuery.parseJSON(this.response);
              if(typeof data.error !== 'undefined') { // Error. !isset(data.error)
                $('#failtext').text(data.description);
                $('#uploadFail').show(); // Vis beskjed om feil
                $('#uploadSuccess').hide(); // Hide success alert
              } else { // Success
                $("#upload-form")[0].reset(); // Reset form
                $('#uploadFail').hide(); // Hide failed upload alert
                $('#uploadSuccess').show(); // Show success alert
              }
            } catch(err) {
              $('#failtext').text("Det oppsto en ukjent feil.");
              $('#uploadFail').show(); // Show fail alert.
              $('#uploadSuccess').hide(); // Hide success alert
            }
          }
        }
      }
      xhr.send(formData);    // Send filen
    }
  });
  
  
  // Delete picture from DB when form for delete is submitted.
  $('.delpic-form').submit(function(e) {
    e.preventDefault();
    
    var videoID = $("#my-player").data('videoid');
    var id = $(this).children('button[name=delete]').val();
    console.log(id);
    
    $.post("takeEdit.php?do=delPic", {id: id, vid: videoID }, function(data) {
      console.log(data);
      if(data == 'done') {
        $('#img' + id).remove();
        if(!$('.imgrow').length) { // Last image was removed.
          $('#imgrow').remove(); // remove the entire DIV.
        }
      }
    });
    
  });
  
}