<?php
/**
 * ReceiveSubtitle.php:
 *
 * Gets subtitles file and stores it in DB and disc
 *
 * See also:
 * UploadSubtitles.php
 * UploadSubtitles.js
 * 
 */
session_start();							// Start the session

require_once 'include/db.php';				// Connect to the database
require_once 'classes/user.inc.php'; 		// Get user class
$user = new User($db); // Create user object.

require_once 'classes/subtitle.inc.php'; // Get Video class.

/*
$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) { // request was sent with ajax, show only content.
	$ajax = true;
}


if(!$ajax) // No header on ajax requests.
	require_once 'include/header.php';
*/
//$dirUpload = 'trans/';
//$uploadedFile= $dirUpload . basename($_FILES['file']['name']);
/*Før:
$fn = $_FILES['file']['name'];
$subtitle = new Subtitle($db);
$subtitle->addSubtitle($fn, $_POST['videoID'], $_POST['languageCode']);
header('Location: player.php?id='.urlencode($_POST['videoID']));
 */
header ("Content-type: application/json");

$fn = $_FILES['file']['name'];
if (isset($_FILES['file']['name'])) {									// Dersom en fil er mottatt
	$fn = $_FILES['file']['name']; // Filnanvn.
	/* Burde ta en ekstra sjekk om $fn er video her ? evt i addVideo()..? (Det gjøres også i JS) */
	$subtitle = new Subtitle($db);; // Subtitle object.
	$res =$subtitle->addSubtitle($fn, $_POST['videoID'], $_POST['languageCode']);
	if(isset($res['success'])) { // Successfull add.
		echo json_encode(array('ok'=>'OK', 'subtitle' => get_object_vars($subtitle)));
		//echo json_encode(array('success'=>'success'));
	} else {
		echo json_encode($res);
		
	}
} else {
	echo json_encode(array('error' => 'error', 'description' => 'Filen ble ikke motatt'));
}










/*

if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadedFile)) { //Dersom en fil er mottat
      $fn = $_FILES['file']['name']; // Filnanvn.
	  $subtitle = new Subtitle($db); // Subtitle object.
	
	  
	 
} else {
    echo "Feil!\n";
}


*/












/*
echo $fn;
echo $_POST['videoID'];
echo $_POST['languageCode'];
*/


