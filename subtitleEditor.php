<?php


/**
 * subtitleEditor.php
 *
 *    Allows to edit the subtitles
 *
 *
 */

session_start();					// Start the session
require_once 'include/db.php'; // Connect to the database
require_once 'classes/user.inc.php'; // Get user class
require_once 'classes/video.inc.php'; // Get video class
require_once 'classes/playlist.inc.php'; // Get playlist class
require_once 'classes/subtitle.inc.php'; // Get subtitle class


$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) { // request was sent with ajax, show only content.
  $ajax = true;
}

$user = new User($db); // Create user object.

$pageTitle = 'Video DB - Edit video.';
if(!$ajax) // No header on ajax requests.
  require_once 'include/header.php';
  
if(!isset($_GET["id"])){
  $msg = array(
    'title' => "Sorry!",
    'text' => "Missing video/playlist ID!"
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

$video = new Video($db, $_GET["id"]); // Create video object

if($video->error) {
  $msg = array(
    'title' => "Sorry!",
    'text' => $video->error
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

if($video->getOwnerID() != $user->getUID()) {
  $msg = array(
    'title' => "Sorry!",
    'text' => 'Kun eier av video kan redigere undertekst.'
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

$pictures = $video->getPictures();

if(!$ajax) {
  ?>
  <div id="main-content">
  <?php
}

?>
<div class="container">
  <div class="page-header">
    <h1><a data-pjax href="player.php?id=<?php echo $video->id ?>"><span id="info-title"><?php echo $video->getTitle();?></span></a> <small>Edit subtitle</small></h1>
  </div>
  
  <div class="row">
    <div class="col-md-8">
      <div  class="embed-responsive embed-responsive-16by9">
        <video 
            id="my-player"           
            class="video-js embed-responsive-item"
            controls
            preload="auto"
            poster="<?php echo $video->getThumb(); ?>"
            data-videoID="<?php echo $video->id; ?>"
            data-userID="<?php echo $user->uid; ?>"
          >
            <source src= <?php echo $video->getPath(); ?> type="video/mp4">
        
        <?php
         $results = array();
         $subtitle=new Subtitle($db);
         $results = $subtitle->findSubtitles($video->id); //Pr�ver � finne subtitles for videoen
        
         foreach($results as $result) {?> 		
        <?php $idLang=$result->idLanguage;?>
        <track default class="subtitle" kind="captions" src="<?php echo $result->getPath();?>" srclang="<?php echo $result->idLanguage;?>" label="<?php echo $result->getLanguage();?>">
         <?php 	
         }
             ?> 
            <p class="vjs-no-js">
              To view this video please enable JavaScript, and consider upgrading to a
              web browser that
              <a href="http://videojs.com/html5-video-support/" target="_blank">
                supports HTML5 video
              </a>
            </p>
        </video>
      </div>
    </div>
    <div class="col-md-4">
      <div id="transcript"></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <form id="saveForm" class="form-inline" role="form">
        <input type="hidden" name="current" value="<?php echo $results[0]->id; ?>" id="currentTrack" >
        <div class="form-group">

          <label for="startTime">Start-tid</label>

            <input size="2" name="startTime" type="number" step="0.001" class="form-control inputs" id="startTime">
        </div>
        <div class="form-group">
     
          <label for="endTime">Slutt-tid</label>

            <input size="2" name="endTime" type="number" step="0.001" class="form-control inputs" id="endTime">

        </div>
        <div class="form-group">
          <label for="cueText">Tekst</label>  
          <input name="cueText" class="form-control inputs" rows="1" id="cueText">
        </div>
        <div class="checkbox">
          <label>
            <input class="inputs" type="checkbox" name="new" value="1"> new?
          </label>
        </div>
        <button type="submit" class="inputs btn btn-default">Save</button>
        <button id="remove" type="button" class="inputs btn btn-default">Remove</button>
        <a class="btn btn-default" href="#" data-toggle="modal" data-target="#myModal2">Add picture</a>
      </form>
    </div>
  </div>
  <?php
  if(!empty($pictures)) { // We got pictures attached.
  ?>
  <div id="imgrow" class="row">
    <div class="col-md-12">
      <h4>Pictures attached to this video.</h4>
      <table id="img-table" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>Picture</th>
            <th>start time</th>
            <th>end time</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
        <?php
        foreach($pictures as $pic) { // For each picture.
          ?>
          <tr class="imgrow" id="img<?php echo $pic['id']; ?>">
            <td>
              <a class="imglink" href="<?php echo $video->getImagePath().$pic['file']; ?>">
                <img style="width: 100px;" src="<?php echo $video->getImagePath().$pic['file']; ?>" >
              </a>
            </td>
            <td><?php echo $pic['startTime']; ?></td>
            <td><?php echo $pic['endTime']; ?></td>
            <td>
              <form class="delpic-form" action="takeEdit.php?do=delPic" method="POST">
                <button type="submit" name="delete" value="<?php echo $pic['id']; ?>" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></button>
              </form>
            </td>
          </tr>
          <?php
        }
        ?>
        </tbody>
      </table>
    </div>
  </div>
  <?php
  }
  ?>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel2">Add Picture</h4>
      </div>
      <div class="modal-body">
        <p>This function adds a picture at the current time in the video.</p>
        <form id="upload-form" action="receivePicture.php" method="post" role="form">
          <input type="hidden" name="vid" value="<?php echo $video->id; ?>" >
          <input type="hidden" name="startTime" value="0" >
          <input type="hidden" name="endTime" value="0" >
          <div class="form-group">
            <label for="duration">Duration (How long the picture is shown)</label>
            <input name="duration" type="number" class="form-control" id="duration" placeholder="Duration" value="">
          </div>
          <div class="form-group">
            <label for="input-id" class="control-label">Select File</label>
            <div id="errorBlock" class="help-block file-error-message" style="display: none;"></div>
            <input id="input-id" name="file" type="file" class="file">
          </div>
        </form>
        <div class="progress">
          <div id="progress" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">
            0%
          </div>
        </div>
        <div id="uploadSuccess" class="alert alert-success" style="display: none;" role="alert"><strong>Congratulations!</strong> File uploaded!</div>
        <div id="uploadFail" class="alert alert-danger" style="display: none;" role="alert"><strong>Sorry!</strong> <span id="failtext"></span></div>
      </div>
    </div>
  </div>
</div>

<?php
$jsIncInline = array('
  initPlayer();
  initSubtitleEditor();
');
require_once 'include/footer.php';
