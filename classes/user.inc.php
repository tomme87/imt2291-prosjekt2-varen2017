<?php
/**
 * The User class handles user login/logout and maintaining of login status.
 * The class accepts a database handle in the constructor and assumes there is
 * a table called users with the columns : id, email, pwd, givenname and surename.
 * For creating new users it is also assumed that the id field is auto incrementet
 * and that the email field has an index that enforces unique email (username).
 * 
 * @author imt2291+tom,manuel
 *
 */
class User {
  var $uid = -1;
  var $admin = 0;
  var $db;
  var $unknownUser = null;
  var $alert = null;
  var $username = null;
  var $name = null;
  var $teacher =null;
  
  /**
   * Constructor class
   *
   * @param PDO $db database object
   */
  function User(PDO $db) {
    $this->db = $db; // Database object.
    if (isset($_GET['logout'])) { // Logging out
      unset ($_SESSION['uid']); // Clear the session variable
      if (isset($_COOKIE['persistent']))
        $this->removePersistentLogin();
    } else if (isset($_POST['email'])) {  // Logging in   
      $sql = "SELECT id, password, admin, username, givenname, surname FROM users WHERE email=?";
      $sth = $db->prepare($sql);  
      $sth->execute (array ($_POST['email']));  // Get user info from the database
      if ($row = $sth->fetch(PDO::FETCH_ASSOC)) { // If user exists
        if (password_verify ($_POST['password'], $row['password'])) { // If correct password
          $_SESSION['uid'] = $row['id'];    // Store user id in session variable
          $this->uid = $row['id'];      // Store user id in object
          $this->admin = $row['admin']; 
          $this->username = $row['username'];
          $this->name = array ('givenname'=>$row['givenname'], 'surname'=>$row['surname']);
          echo $_POST['remember'];
          if (isset($_POST['remember'])&&   // Logging in and remember login
              $_POST['remember']=='1')
            $this->persistentLogin();
          header("Location: index.php");
        } else {  // Bad password
          $this->unknownUser = 'Uknown username/password';
          // Note, never say bad password, then you confirm the user exists
        }
      } else {    // Unknow user
        $this->unknownUser = 'Uknown username/password';
        // Same as for bad password
      }
    } else if (isset($_SESSION['uid'])) { // A user is logged in
      $this->maintainSession();
    } else if (isset($_COOKIE['persistent'])&&$_COOKIE['persistent']!='') { //  Do autologin
      $this->autologin();
    }
  }
  
  /**
   * Called when successfully logged in and remember me is checked
   * Creates a new row in the table persistent_login.
   * 
   * Note, this creates a new persistent login session
   */
  function persistentLogin () {
    // Create an unique identifier for this login
    //$headers = apache_request_headers (); // Funker ikke på min PC (Jeg kjører ikke apache). Bruker $_SERVER['HTTP_USER_AGENT'] i stede.
    $series = md5(session_id());
    
    // Create a unique identifier that will change for each 
    // subsequent auto login
    $token = md5($series.date(DATE_RFC2822).$this->uid.$_SERVER['HTTP_USER_AGENT']);
    
    $encryptedToken = password_hash ($token, PASSWORD_DEFAULT);
    
    // Store the data in the browser
    setcookie('persistent', "{$this->uid}:$series$token", strtotime( '+30 days' ));
    
    // Store the data in the database
    $sql = "INSERT INTO persistent_login (uid, series, token)
    VALUES (?, ?, ?)";
    
    
    $sth = $this->db->prepare ($sql);
    // Note, the token that changes is stored encrypted
    $sth->execute (array ($this->uid, $series, $encryptedToken));
  }
  
  /**
   * Call this method when a user logs out and has persistent login
   * enabled.
   * This will remove the information about this persistent login
   * session both from the user browser and from the database.
   */
  function removePersistentLogin () {
    $loginData = split(":", $_COOKIE['persistent']);
    $uid = $loginData[0];
    $series = substr($loginData[1], 0, 32);
    
    $sql = 'DELETE from persistent_login WHERE uid=? and series=?';
    $sth = $this->db->prepare($sql);
    $sth->execute (array ($uid, $series));
    setcookie('persistent');
  }
  
  /**
   * This method is called if a $_SESSION['uid'] exists
   * Used to get user information from the database.
   */
  function maintainSession () {
    $sql = "SELECT admin, teacher, username, givenname, surname FROM users WHERE id=?";
    $sth = $this->db->prepare($sql);
    $sth->execute (array ($_SESSION['uid'])); // Find user information from the database
    if ($row = $sth->fetch()) {         // User found
      $this->uid = $_SESSION['uid'];      // Store user id in object
      $this->admin = $row['admin'];
      $this->teacher = $row['teacher'];
      $this->username = $row['username'];
      $this->name = array ('givenname'=>$row['givenname'], 'surname'=>$row['surname']);
    } else {                  // No such user
      unset ($_SESSION['uid']);       // Remove user id from session
    }
  }
  
  /**
   * This function is called if no other login exists but 
   * an persistent login cookie was set.
   * 
   * Attempts an auto login. If uid and series id exists but
   * the token does not match an intrucion alert will be raised 
   * and all existing login information will be removed.
   */
  function autoLogin () {
    $loginData = split(":", $_COOKIE['persistent']);
    if (count($loginData)==0)
      return;
    $uid = $loginData[0];
    $series = substr($loginData[1], 0, 32);
    $token = substr($loginData[1], 32, 32);
    // echo ("{$_COOKIE['persistent']}</br>$uid</br>$series</br>$token</br>");  // Used for debug purposes
    $sql = 'SELECT uid, series, token FROM persistent_login WHERE uid=? AND series=?';
    $sth = $this->db->prepare($sql);
    $sth->execute (array($uid, $series));
    if ($row = $sth->fetch()) {
      if (password_verify($token, $row['token'])) {
        $_SESSION['uid'] = $uid;
        $this->uid = $uid;

        // Create a unique identifier that will change for each
        // subsequent auto login
        $token = md5($series.date(DATE_RFC2822).$this->uid.$_SERVER['HTTP_USER_AGENT']);
        
        $encryptedToken = password_hash ($token, PASSWORD_DEFAULT);
                
        // Store new data in the browser
        setcookie('persistent', "{$this->uid}:$series$token", strtotime( '+30 days' ));
        
        // Update the data in the database
        $sql = "UPDATE persistent_login SET token=? WHERE uid=? AND series=?";
        
        $sth = $this->db->prepare ($sql);
        // Note, the token that changes is stored encrypted
        $sth->execute (array ($encryptedToken, $this->uid, $series));
        $this->maintainSession ();
      } else { // Userid/series id found but no matching token
        // This suggests a stolen cookie, some one has gained access 
        // as the user.
        $sql = "DELETE FROM persistent_login WHERE uid=?";
        
        $sth = $this->db->prepare ($sql);
        // Note, the token that changes is stored encrypted
        $sth->execute (array ($uid));
        
        // NB! SJEKK!!
        $this->alert = "Possible security breach. All sessions terminated. You should log on and change your password.";
      }
    } // No userid/series identificator found, ignoring the information
  }
  
  /**
   * Use this function to get the user id for the logged in user.
   * Returns -1 if no user is logged in, or the id of the logged in user.
   * 
   * @return long integer with user id, or -1 if no user is logged in.
   */
  function getUID() {
    return $this->uid;
  }
  
  /**
   * This method returns true if a user is logged in, false if no 
   * user is logged in.
   * 
   * @return boolean value of true if a user is logged in, false if no user is logged in.
   */
  function isLoggedIn() {
    return ($this->uid > -1); // return true if userid > -1
  }
  
  /**
   * This method returns true if a user is admin, false if no
   *
   * @return boolean
   */
  function isAdmin() {
    return ($this->admin == 1); // True if admin is 1
  }
  
  /**
   * Makes a user admin.
   *
   * @param int $id User id.
   */
  function makeAdmin($id) {
    $sql = 'UPDATE users SET admin = 1 WHERE id = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    $this->admin = 1;
  }
  
  function makeTeacher($id) {
  	$sql = 'UPDATE users SET teacher = 1 WHERE id = ?';
  	$sth = $this->db->prepare($sql);
  	$sth->execute(array($id));
  	$this->teacher = 1;
  }
  
  /**
   * This method returns true if a user is teacher, false if no
   *
   * @return boolean
   */
  function isTeacher() {
  	return ($this->teacher == 1); // True if admin is 1
  }
  
  function makeStudent($id) {
  	$sql = 'UPDATE users SET teacher = 0 WHERE id = ?';
  	$sth = $this->db->prepare($sql);
  	$sth->execute(array($id));
  	$this->teacher = 0;
  }
  
  /**
	 * Method used to add a user to the database. Takes username, password, first and
	 * last name as parameters and attempts to add the user to the database.
	 * 
	 * On success an array with the element 'success' is returned. If it
	 * failed (probably because the username was taken) an array with 
	 * two items is return, 'error' is set and 'description' gives the reason
	 * for the failure.
	 * 
	 * @param string $username the username of the new user
	 * @param string $pwd the password for the new user
	 * @param string $givenname the first name of the new user
	 * @param string $surname the last name of the new user
   * @param string $email email of the user
	 * @return array that indicates failure or success. 
	 */
  function addUser ($username, $pwd, $givenname, $surname, $email) {
		$sql = 'INSERT INTO users (username, email, password, givenname, surname) VALUES (?, ?, ?, ?, ?)';
    $sth = $this->db->prepare ($sql);
    // Add the user to the database
    $sth->execute (array ($username, $email, password_hash($pwd, PASSWORD_DEFAULT), $givenname, $surname));
    if ($sth->rowCount()==0) {	// If unable to create user
      $this->unknownUser = 'email or username already registered';
      return (array ('error'=>'error', 'description'=>'email or username already registered'));
    }	
    $this->uid = $this->db->lastInsertId();
    if($this->uid == 1) $this->makeAdmin($this->uid); // Make first user admin.
    echo $this->uid;
    $this->name = array ('givenname'=>$givenname, 'surname'=>$surname);
	    
		$_SESSION['uid'] = $this->uid;
	  return (array ('success'=>'success'));
	}
  
  /**
   * Function that searches for users.
   *
   * @param string $search the search string
   * @return array with users. (empty on no result)
   */
  function find($search = '_') {
    $sql = 'SELECT id, email, username, givenname, surname, teacher, admin FROM users WHERE LOWER(username) LIKE ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array('%'.strtolower($search).'%'));
    $results = array();
    if ($sth->rowCount()==0) { // No results
      return $results;
    }
    $results = $sth->fetchAll(PDO::FETCH_ASSOC);
    
    return $results;
  }
  
  /**
   * This deletes a user from the database. Only admin can run this function.
   *
   * @param int $id user ID
   * @return array that indicates failure or success. 
   */
  function delete($id) {
    if(!$this->isAdmin()) {
      return (array('error'=>'error', 'description'=>"You need to be administrator to delete users!"));
    } elseif($this->uid == $id) {
      return (array('error'=>'error', 'description'=>"You can't delete yourself!!"));
    }
    
    $sql = 'DELETE FROM users WHERE id = ? AND admin = 0'; // admin = 0 cause can't delete admins
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    if ($sth->rowCount()==0) {	// If unable to delete user
      return (array('error'=>'error', 'description'=>"Unable to delete user."));
    }
    return (array ('success'=>'success'));
  }
  
}