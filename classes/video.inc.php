<?php
/**
 * This class handles video objects including getting video data
 * adding video, removing video etc.
 * 
 * @author tom+manuel
 */
class Video {
  var $id = -1;
  var $filename = null;
  var $title = null;
  var $description = null;
  var $userid = null;
  private $db;
  var $error = null;
  private $thumb = null; // RAW image file.
  private $username = null;
  
  const UPLOAD_DIR = 'uploads/'; // With trailing /
  const IMAGE_DIR = 'images/';
  const TEMP_DIR = 'temp/';
  const FFMPEG_BIN = 'bin'.DIRECTORY_SEPARATOR.'ffmpeg';
  const FFPROBE_BIN = 'bin'.DIRECTORY_SEPARATOR.'ffprobe';
  //const FFMPEG_BIN = 'bin\ffmpeg';
  //const FFPROBE_BIN = 'bin\ffprobe';
  
  /**
   * The constructor. Get video info from DB if $id set.
   *  
   * @param PDO $db database object.
   * @param int $id the video ID (optional).
   * @param string $filename
   * @param string $title
   * @param string $description
   * @param int $userid
   * @param string $thumb RAW image.
   */
  function Video(PDO $db, $id = null, $filename = null, $title = null, $description = null, $userid = null, $thumb = null) {
    $this->db = $db; // Database object.
    if($id && $filename && $title && $description && $userid) { // If all variables set, just add them.
      $this->id = $id;
      $this->filename = $filename;
      $this->title = $title;
      $this->description = $description;
      $this->userid = $userid;
      $this->thumb = $thumb;
    } elseif($id) { // Only ID set. get from DB
      $sql = 'SELECT id, filename, title, description, userID, thumb FROM videos WHERE id = ?';
      $sth = $this->db->prepare($sql);
      $sth->execute(array($id));
      if ($row = $sth->fetch(PDO::FETCH_ASSOC)) { // If video exists
        $this->id = $row['id'];
        $this->filename = $row['filename'];
        $this->title = $row['title'];
        $this->description = $row['description'];
        $this->userid = $row['userID'];
        $this->thumb = $row['thumb'];
      } else {
        $this->error = "Unable to get get video from ID.";
        //return false;
      }
      
    }
  }
  
  /**
   * Get title with possibility to only get first chars
   *
   * @param int $first how many chars to get. (optional)
   * @return string title
   */
  function getTitle($first = null) {
    if($first !== null) {
      return mb_substr($this->title, 0, $first).(strlen($this->title) > $first ? '...' : '');
    }
    return $this->title;
  }
  
  /**
   * Get description with possibility to only get first chars
   *
   * @param int $first how many chars to get. (optional)
   * @return string description
   */
  function getDescription($first = null) {
    if($first !== null) {
      return mb_substr($this->description, 0, $first).(strlen($this->description) > $first ? '...' : '');
    }
    return $this->description;
  }
  
  /**
   * Get the path to the file.
   *
   * @return string
   */
  function getPath() {
    return self::UPLOAD_DIR . $this->filename;
  }
  
  /**
   * Get the path to image dir.
   *
   * @return string
   */
  function getImagePath() {
    return self::IMAGE_DIR;
  }
  
  function getID() {
  	return $this->id;
  }
  

  
  /**
   * Get username of uploader.
   *
   * @return string A username.
   */
  function getUsername() {
    if($this->username) return $this->username;
    
    $sql = 'SELECT username FROM users WHERE id = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($this->userid));
    if ($row = $sth->fetch(PDO::FETCH_ASSOC)) { // If video exists
      return $row['username'];
    } else {
      return null;
    }
  }
  
  /**
   * Get ID of uploader.
   *
   * @return int ID
   */
  function getOwnerID() {
    return $this->userid;
  }
  
  /**
   * Get  number of reproductions of av video
   *
   * @return int. Number of reproductions.
   */
    
  function getNumberOfViews() {
  	
 
  	$sql= 'SELECT COUNT(*) as total FROM plays WHERE videoID = ?';
  	$sth = $this->db->prepare($sql);
  	$sth->execute(array($this->id));
  	if ($row = $sth->fetch(PDO::FETCH_ASSOC)) { // If video exists
  		return  $row['total'];
  	} else {
  		return null;
  	}
  }
  	
  
  function getDetailsOfViews() {
  	  	  	
  	$sql= 'SELECT userID, username, givenname, surname, COUNT(*) AS views  FROM plays  
			LEFT OUTER JOIN users AS us ON userID=us.id
			Where videoID = ? GROUP BY userID';
  	$sth = $this->db->prepare($sql);
  	$sth->execute(array($this->id));
  	$results = array();
  	if ($sth->rowCount()==0) { // No results
  		return $results;
  		
  	}
  	$results = $sth->fetchAll(PDO::FETCH_ASSOC);  		
  	return $results;
  }
  
 
  
  
  /**
   * Adds the thumbnail to the database
   *
   * @return boolean if the thumb was added successfully.
   */
  function addThumb() {
    if($this->id == -1 || $this->thumb === null) return false; // We got nothing to add
    $sql = "UPDATE videos SET thumb = ? WHERE id = ?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($this->thumb, $this->id));
    if ($sth->rowCount()==0) { // Unable to add thumb
      return false;
    }
    return true;
  }
  
  function addPicVTT($startTime, $endTime, $file) {
    $sql = "INSERT INTO images (file, startTime, endTime, videoID) VALUES (?, ?, ?, ?)";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($file, $startTime, $endTime, $this->id));
    if ($sth->rowCount()==0) { // Unable to add
      $this->error = "Unable to add picture. File probably exists, or a file with the same name exists.";
      return array('error' => 'error', 'description' => $this->error);
    }
    
    if(isset($_FILES['file'])) { // Probably from a file upload. save the file to disk.
      $path = $this->getImagePath().$file; // Where to save the file.
      if(!file_exists($path)) { // Filen eksiterer ikke
        file_put_contents(						// Ta i mot filen og lagre den i images katalogen
          $path,					// Her må en ta høyde for dupliserte filnavn	
          file_get_contents($_FILES['file']['tmp_name'])
        );
      } else { // Filen ekisterer fra før. (Burde som regel bli fanget opp ved innleggelse i database)
        $this->error = "Info added to DB, but file did not get saved.";
        return array('error' => 'error', 'description' => $this->error);
      }
    }
    
    return(array('success'=>'success'));
  }
  
  function formatTime($t) {
    $tms = $t * 1000;
    $ms = (int)($tms%1000);
    $s = (int)(($tms/1000)%60);
    $m = (int)(($tms/(1000*60))%60);
    $h = (int)(($tms/(1000*60*60))%24);
    
    $h = ($h < 10) ? "0" . $h : $h;
    $m = ($m < 10) ? "0" . $m : $m;
    $s = ($s < 10) ? "0" . $s : $s;
    $ms = ($ms < 10) ? "00" . $ms : $ms;
    $ms = ($ms < 100 && $ms >= 10) ? "0" . $ms : $ms;
    
    return $h . ':' . $m . ':' . $s . '.' . $ms;
  }
  
  /**
   * Get pictures assosiated to this video.
   *
   * @return array picture objects
   */
  function getPictures() {
    $sql = 'SELECT id, startTime, endTime, file FROM images WHERE videoID = ? ORDER BY startTime';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($this->id));
    
    $results = array();
  	if ($sth->rowCount()==0) { // No results
  		return $results;
  		
  	}
  	$results = $sth->fetchAll(PDO::FETCH_ASSOC);  		
  	return $results;
  }
  
  /**
   * Function that delete a picture from DB.
   */
  function delPic($id) {
    $sql = 'SELECT file FROM images WHERE videoID = ? AND id = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($this->id, $id));
    
    if ($sth->rowCount()==0) { // No results
  		return false;
  	}
    $row = $sth->fetch(PDO::FETCH_ASSOC);
    
    unlink($this->getImagePath().$row['file']); // delete file.
    
    // Delete from DB.
    $sql = 'DELETE FROM images WHERE videoID = ? AND id = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($this->id, $id));
    return true;
  }
  
  /**
   * Retrieves pictures from db and creates VTT.
   *
   * @return string WEBVTT format
   */
  function getPicVTT() {
    $sql = 'SELECT startTime, endTime, file FROM images WHERE videoID = ? ORDER BY startTime';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($this->id));
    $results = array();
    
    $vtt = "WEBVTT\n\n";
    if ($sth->rowCount()!=0) { // got results
      $index = 1;
      while($res = $sth->fetch(PDO::FETCH_ASSOC)) {
        //$results[] = new Video($this->db, $res['id'], $res['filename'], $res['title'], $res['description'], $res['userID'], $res['thumb']);
        $vtt .= $index++ . "\n" . $this->formatTime($res['startTime']) . ' --> ' . $this->formatTime($res['endTime']) . "\n" . $this->getImagePath().$res['file'] . "\n\n";
      }
    }
    
    return $vtt;
  }
  
  /**
   * Uses ffmpeg to create thumbnail of the video and adds it to the database.
   * ffprobe is used to find duration of video to calculate where in the video to take thumbnail.
   * 
   * $return array that indicates failure or success.
   */
  function createThumb() {
    $offset = 1;
    
    // Find duration.
    $command = self::FFPROBE_BIN . ' -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ' . escapeshellarg($this->getPath()) . ' 2>&1';
    $var = exec($command, $out, $ret);
    if($ret === 0) { // Duration found
      if ($out[0] < 4 && $out[0] > 0) $offset = 1;
      else $offset = $out[0] / 4;
    } elseif($ret == 127) { // Command not found
      $this->error = "Unable to find duration. ffprobe not found.";
      return array('error' => 'error', 'description' => $this->error);
    } else {
	  $this->error = "Unable to find duration of video. error code: $ret".implode(',',$out);
      return array('error' => 'error', 'description' => $this->error);
    }
    $offset = (int) $offset;
    
    // Create thumbnail.
    $imgPath = self::TEMP_DIR . $this->id . '.png';
    $command = self::FFMPEG_BIN . ' -y -i ' . escapeshellarg($this->getPath()) . ' -ss ' . $offset . ' -vframes 1 '. escapeshellarg($imgPath) . ' 2>&1';
    $var = exec($command, $out, $ret); // Run ffmpeg command to create thumbnail.
    
    if($ret === 0) { // Success
      $this->thumb = file_get_contents($imgPath);
      if($this->addThumb()) { // Add thumb to database.
        unlink($imgPath); // We can delete file from temp.
        return(array('success'=>'success'));
      } else { // Thumb not added
        $this->error = "Unable to add thumbnail to database.";
        return array('error' => 'error', 'description' => $this->error);
      }
    } elseif($ret == 127) { // Command not found
      $this->error = "Unable to create thumbnail. ffmpeg not found.";
      return array('error' => 'error', 'description' => $this->error);
    } else { // Something else went wrong
      $this->error = "Unable to create thumbnail. error code: $ret";
      return array('error' => 'error', 'description' => $this->error);
    }
  }
  
  /*
   * Function the get the base64 encoded format of the thumb
   * ready to put in a <img src=..> tag.
   *
   * @return string in base64 format
   */
  function getThumb() {
    if(isset($this->thumb) && $this->thumb) {
      return 'data:image/png;base64,' . base64_encode($this->thumb);
    } else {
      return '';
    }
  }
  
  /**
   * Add a new video to the database.
   *
   * @param string $filename the video filename.
   * @param string $title the video title.
   * @param string $description the video description.
   * @param int $userid the user ID of uploader (Video owner).
   * @param boolean $nosave if true, we will not atempt to save the file to disk
   * @return array that indicates failure or success.
   */
  function addVideo($filename, $title, $description, $userid, $nosave = false) {
    $sql = "INSERT INTO videos (filename, title, description, userID) VALUES (?, ?, ?, ?)";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($filename, $title, $description, $userid));
    if ($sth->rowCount()==0) { // Unable to add video
      $this->error = "Unable to add video. File probably exists, or a file with the same name exists.";
      return array('error' => 'error', 'description' => $this->error);
    }
    $this->id = $this->db->lastInsertId();
    $this->filename = $filename;
    $this->title = $title;
    $this->description = $description;
    $this->userid = $userid;
    
    if(isset($_FILES['file'])) { // Probably from a file upload. save the file to disk.
      $path = $this->getPath(); // Where to save the file.
      if(!file_exists($path)) { // Filen eksiterer ikke
        file_put_contents(						// Ta i mot filen og lagre den i uploads katalogen
          $path,					// Her må en ta høyde for dupliserte filnavn	
          file_get_contents($_FILES['file']['tmp_name'])
        );
      } else { // Filen ekisterer fra før. (Burde som regel bli fanget opp ved innleggelse i database)
        $this->error = "Info added to DB, but file did not get saved.";
        return array('error' => 'error', 'description' => $this->error);
      }
    }
    
    // Her burde noe post-prosessering av video skje. hente video-info, lage thumbnails osv. Men det må skje i "bakgrunn", slik at det ikke blokker scriptet.
    $res = $this->createThumb();
    if(!isset($res['success'])) return $res;
    
    
    return(array('success'=>'success'));
  }
  
  /**
   * Searches for av video in the database and returns them.
   * 
   * @param string $search The search string.
   * @return array with Video objects. empty array when nothing found.
   */
  function find($search = '_') {
    $search = strtolower($search);
    $sql = 'SELECT id, filename, title, description, userID, thumb FROM videos WHERE LOWER(title) LIKE ? OR LOWER(description) LIKE ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array('%'.$search.'%', '%'.$search.'%'));
    $results = array();
    if ($sth->rowCount()==0) { // No results
      return $results;
    }
    while($res = $sth->fetch(PDO::FETCH_ASSOC)) {
      $results[] = new Video($this->db, $res['id'], $res['filename'], $res['title'], $res['description'], $res['userID'], $res['thumb']);
    }
    
    return $results;
  }
  
  /**
   * Searches for video based on userid and returns them.
   * 
   * @param int $id User ID.
   * @return array with Video objects. empty array when nothing found.
   */
  function findByUser($id) {
    $sql = 'SELECT id, filename, title, description, userID, thumb FROM videos WHERE userID = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    $results = array();
    if ($sth->rowCount()==0) { // No results
      return $results;
    }
    while($res = $sth->fetch(PDO::FETCH_ASSOC)) {
      $results[] = new Video($this->db, $res['id'], $res['filename'], $res['title'], $res['description'], $res['userID'], $res['thumb']);
    }
    
    return $results;
  }
  
  /**
   * Edit title and description
   *
   * @param string $title video title
   * @param string $description video description.
   */
  function edit($title, $description) {
    $sql = 'UPDATE videos SET title = ?, description = ? WHERE id = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($title, $description, $this->id));
    if ($sth->rowCount()==0) { // No results
      return false;
    }
    $this->title = $title;
    $this->description = $description;
    return true;
  }

}