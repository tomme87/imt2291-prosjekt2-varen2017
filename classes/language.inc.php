<?php


/**
 * This class handles language objects including 
 * 
 * 
 * @author tom+manuel
 */
class Language {
  var $id = null;
  var $language = null;
  var $message=null;
  private $db;

  
  /**
   * The constructor. 
   * Add the info of new languages in DB  
   * @param PDO $db database object.
   * @param int $id the language ID (optional).
   * @param string $language
   */
  function Language(PDO $db, $id=null, $language=null) {
    $this->db = $db; // Database object.

	
    if($id && $language) { // If all variables set, just add them.
      $this->id = $id;
      $this->language = $language;
    
      $sql = 'INSERT INTO languages (id, language) VALUES (?, ?)';
      $sth = $db->prepare($sql);
      $sth->execute(array($id, $language));
		if ($sth->rowCount()==0) { // Unable to add language
			$this->message = 0;
			//$this->error = "There was no possible to add this Language.";
			//return array('error' => 'error', 'description' => $this->error);
		}else{
		
			$this->message = 1;
			//return(array('success'=>'success'));
		}    	
		
	}
      
 }  //End of Language

 
 
  
  /**
   * Gets alle languages
   */
  function getAllLanguages(){
    $sql = 'SELECT id, language FROM languages';
    $sth = $this->db->prepare($sql);
    $sth->execute(array());
    $results = array();
    if ($sth->rowCount()==0) { // No results
      return $results;
    }

    $results = $sth->fetchAll(PDO::FETCH_ASSOC);

    return $results;	
  }

    
  

}//End of class Language
  
