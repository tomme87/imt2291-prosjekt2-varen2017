<?php
/**
 * This class handles playlist objects
 * creates playlists, add videos to playlist, removes videos from playlist
 */

require_once 'video.inc.php'; // Get video class
 
class Playlist {
  var $id = -1;
  var $userid = null;
  var $title = null;
  var $videos = array();
  var $error = null;
  
  /**
   * The constructor. Get playlist info from DB if $id set.
   *  
   * @param PDO $db database object.
   * @param int $id the playlist ID (optional).
   */
  function Playlist(PDO $db, $id = null) {
    $this->db = $db; // Database object.
    if($id) {
      $sql = 'SELECT id, userID, title FROM playlist WHERE id = ?';
      $sth = $this->db->prepare($sql);
      $sth->execute(array($id));
      if ($row = $sth->fetch(PDO::FETCH_ASSOC)) { // Playlist exists
        $this->id = $row['id'];
        $this->userid = $row['userID'];
        $this->title = $row['title'];
        $this->populateVideos(); // Add videos to videos array from DB.
      } else {
        $this->error = "Unable to get playlist.";
      }
    }
  }
  
  /**
   * This method returns true if a playlist is created, false if no 
   * 
   * @return boolean value of true or false.
   */
  function isCreated() {
    return ($this->id > -1); // return true if userid > -1
  }
  
  function getVideos() {
    return $this->videos;
  }
  
  /**
   * Fetch videos from db and populate the video array with video-objects.
   */
  function populateVideos() {
    if($this->id == -1) return false;
    
    $sql = 'SELECT videoID, position FROM playlist_videos WHERE playlistID = ? ORDER BY position';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($this->id));
    if ($sth->rowCount()==0) { // No results
      $this->videos = array();
    } else {
      while($res = $sth->fetch(PDO::FETCH_ASSOC)) {
        $this->videos[] = array(
          'video' => new Video($this->db, $res['videoID']), // This could be more efficient.
          'position' => $res['position']
        ); 
      }
    }
  }
  
  /**
   * Creates a new playlist. Only available if this is an "empty" object.
   *
   * @param string $title playlist title
   * @param int $userid owner of the playlist.
   */
  function create($title, $userid) {
    if($this->id != -1) return false; // Only available for new playlists
    
    $sql = 'INSERT INTO playlist (userID, title) VALUES (?, ?)';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($userid, $title));
    if ($sth->rowCount()==0) { // Unable to add playlist
      $this->error = "Unable to add playlist.";
      return array('error' => 'error', 'description' => $this->error);
    }
    $this->id = $this->db->lastInsertId();
    $this->userid = $userid;
    $this->title = $title;
    
    return(array('success'=>'success'));
  }
  
  /**
   * gets the last posistion number.
   */
  function lastVideoPos() {
    return count($this->videos);
  }
  
  /**
   * Add a video to end of playlist.
   *
   * @param int $id videoID
   */
  function addVideo($id) {
    if($this->id == -1) return false; // Need to be a playlist created
    
    $sql = 'INSERT INTO playlist_videos (playlistID, videoID, position) VALUES (?, ?, ?)';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($this->id, $id, $this->lastVideoPos()+1));
    if ($sth->rowCount()==0) { // Unable to add video to playlist
      $this->error = "Unable to add video to playlist.";
      return array('error' => 'error', 'description' => $this->error);
    }
    $this->videos[] = array(
      'video' => new Video($this->db, $id), // This could be more efficient.
      'position' => $this->lastVideoPos()+1
    ); 
  }
  
  /**
   * Find playlists by userid
   *
   * @param int $id userID
   * @return array with playlist
   */
  function getPlaylistsByUser($id) {
    $sql = 'SELECT id, title FROM playlist WHERE userID = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    $results = array();
    if ($sth->rowCount()==0) { // No results
      return $results;
    }
    while($res = $sth->fetch(PDO::FETCH_ASSOC)) {
      $results[] = new Playlist($this->db, $res['id']);
    }
    
    return $results;
  }
  
  /**
   * Find all playlists
   *
   * @return array with playlist
   */
  function getAll() {
    $sql = 'SELECT id, title FROM playlist';
    $sth = $this->db->prepare($sql);
    $sth->execute();
    $results = array();
    if ($sth->rowCount()==0) { // No results
      return $results;
    }
    while($res = $sth->fetch(PDO::FETCH_ASSOC)) {
      $results[] = new Playlist($this->db, $res['id']);
    }
    
    return $results;
  }
  
  /**
   * Find out if a video is in this playlist.
   *
   * @param int $vid ID of the video.
   * @return boolean
   */
  function isInPlaylist($vid) {
    foreach($this->videos as $video) {
      if($video['video']->id == $vid) return true;
    }
    return false;
  }
  
  /**
   * Updates the position of videos in the playlist
   *
   * @param array $videos videos containng array with id, new pos and old pos.
   */
  function updateVideoPosition($videos) {
    if(count($videos) != $this->lastVideoPos()) { // we have to update all videos.
      return false;
    }
    $sql = 'UPDATE playlist_videos SET position = ? WHERE playlistID = ? AND videoID = ?';
    $sth = $this->db->prepare($sql);
    foreach($videos as $video) {
      $sth->execute(array($video['newPos'], $this->id, $video['vid']));
    }
    return true;
  }
  
  /**
   * Deletes a video from playlist
   *
   * @param int $id Video ID.
   */
  function removeVideo($id) {
    $sql = 'DELETE FROM playlist_videos WHERE playlistID = ? AND videoID = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($this->id, $id));
    /* Should remove from array here */
    return true;
  }
}