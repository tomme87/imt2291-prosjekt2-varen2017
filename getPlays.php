<?php

/**
 *
 * getPlays.php
 *
 * Register that a video has been watched 
 *
 *
 */


session_start();          // Start the session
require_once 'include/db.php'; // Connect to the database



function registerPlay(PDO $db, $videoID, $userID){
	
	$sql = "INSERT INTO plays (videoID, userID) VALUES (?, ?)";
	$sth = $db->prepare($sql);
	$sth->execute(array($videoID, $userID));
	if ($sth->rowCount()==0) { // Unable to add video
		$this->error = "Unable to add video. File probably exists, or a file with the same name exists.";
		return array('error' => 'error', 'description' => $this->error);
	}
	
}
echo  $_POST['videoID'];
echo  $_POST['userID'];
registerPlay($db, $_POST['videoID'], $_POST['userID']);