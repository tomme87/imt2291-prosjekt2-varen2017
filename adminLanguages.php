<?php
/**
 * 
 * AdminLanguages.php
 * 
 * This page is only available to Admin user
 * Displays a table of all languages available in DB
 * Here it will be possible to add more languages
 * 
 * Sources:
 *   datatables: https://datatables.net ( https://github.com/DataTables/DataTables )
 */

session_start();         				 	// Start the session
require_once 'include/db.php'; 				// Connect to the database
require_once 'classes/user.inc.php';		// Get user class
require_once 'classes/language.inc.php'; 	// Get language class

$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) {	    // request was sent with ajax, show only content.
	$ajax = true;
}


$user = new User($db);	      // Create user object.
$language=new Language ($db); //Create Language object

$pageTitle = 'Video DB - Language list'; // Finn på no :-P

if(!$ajax) // No header on ajax requests.
	require_once 'include/header.php';
//$cssInc = array("jquery.dataTables.css", "dataTables.bootstrap.min.css"); // Ekstra .css filer å inkludere.
//require_once 'include/header.php';

if(!$user->isAdmin()) { // User is NOT admin.
  $msg = array(
    'title' => "Sorry!",
    'text' => "You have to be admin to view userlist!."
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

$results = $language->getAllLanguages();


if(count($results)==0&&!$user->isAdmin()) {
  $msg = array(
    'title' => "Obs!",
    'text' => "Administrator has not allowed any language yet!."
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

if(!$ajax) {
?>
<div id="main-content">
<?php
}
if($ajax && isset($cssInc)) {
  foreach($cssInc as $c) {
    echo "<link rel=\"stylesheet\" href=\"css/$c\" />\n";
  }
}
?>

<div class="container">

   <div class="row" style="margin-top:20px;">
    <div  id="tableContainer" class="col-md-6 col-xs-6">
	
		<div class="page-header">
			<h1>Your Languages </h1>
		</div>
    <?php
    
  
    if(count($results)) {
    ?>
      <table id="user-table" class="table table-hover">
       <?php
        $results = $language->getAllLanguages();
        ?>
        <thead>
          <tr>
            <th>ID</th>
            <th>LANGUAGE</th>
           
          </tr>
        </thead>
        <tbody>
        <?php
        foreach($results as $result) {
		  echo "<tr>";
          echo '<td>'.$result['id']."</td>\n";
          echo '<td>'.$result['language']."</td>\n";      
	      echo  "</tr>";
        
        }
        ?>
        </tbody>
      </table>
      <?php 
      } else {
        echo '<div style="margin-top:20px;" class="alert alert-danger" role="alert"><strong>Sorry!</strong> No languages found.</div>';
      }
      ?>
    </div>
	
	
	 <div class="col-md-6 col-xs-6">		
		<div class="page-header">
			<h1>Add More Languages </h1>
		</div>	
		
		  <form class="form-inline" id="addlang-form">
			<label class="sr-only" for="inlineFormInput">Language ID</label>		
			<input id="languageID" type="text" class="form-control mb-2 mr-sm-2 mb-sm-0"  name="languageID" placeholder="Language ID">
			<input id="languageName"type="text" class="form-control mb-2 mr-sm-2 mb-sm-0"  name="languageName" placeholder="Language Name">
			<button id="submitButton" type="submit" class="btn btn-primary">Submit</button>
		 </form>
		
		
		
		   <div  class="alert alert-success" id="success"  alert-dismissible" role="alert" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Success! </strong > Language code <strong id="LanguageCodeS"> </strong> was saved.
					
		 </div>	
		
		  <div  class="alert alert-danger" id="error" alert-dismissible" role="alert" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>OBS! </strong> Language code <strong id="LanguageCodeF"> </strong> was not saved. Maybe it was saved before.					 
		</div>	
		
		 </div> 
	  </div>
  
</div>
  
  
  	
	

	
<?php
			





//$jsInc = array("jquery.dataTables.js", "dataTables.bootstrap.min.js", "users.js"); // Ekstra .js filer å inkludere.
$jsIncInline = array('$("#user-table").DataTable(); initAdminLanguages();
');

require_once 'include/footer.php';