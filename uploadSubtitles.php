<?php


/**
 * uploadSubtitles.php:
 *
 *   The user must select one of the allowed languages and
 *   Select subtitle file to be uploaded
 *    
 * Se also:
 * 
 *     uploadSubtitles.js
 *
 *
 *
 */
session_start();          // Start the session
require_once 'include/db.php'; // Connect to the database
require_once 'classes/user.inc.php'; // Get user class
require_once 'classes/language.inc.php'; // Get language class
require_once 'classes/video.inc.php'; // Get video class


$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) { // request was sent with ajax, show only content.
	$ajax = true;
}
$user = new User($db); // Create user object.

$language=new Language ($db); //Create Language object

$pageTitle = 'Video DB - Upload subtitle'; // Finn på no :-P

if(!$ajax) // No header on ajax requests.
	require_once 'include/header.php';

//$cssInc = array("fileinput.min.css"); // Ekstra .css filer å inkludere.


$results = array();


//Sjekk om brukeren er logget på
if(!$user->isLoggedIn()) {
  $msg = array(
    'title' => "Sorry!",
    'text' => "You have to be logged in to upload videos!."
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}


//Sjekk om vi har fått en video ID
if(!isset($_GET["id"])){
  $msg = array(
    'title' => "Sorry!",
    'text' => "Missing video ID!"
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}


//Bør også sjekkes om brukere eier videoen
$video = new Video($db, $_GET["id"]); // Create video object
if($video->userid != $user->uid && !$user->isAdmin()) { // need to be Owner or admin
  $msg = array(
    'title' => "Sorry!",
    'text' => "Ingen tilgang!"
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

//Bør også sjekkes om videoen finnes.
if($video->error) {
  $msg = array(
    'title' => "Sorry!",
    'text' => "Fant ikke video.!"
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

$results = $language->getAllLanguages();



if(count($results)==0) {
  $msg = array(
    'title' => "Obs!",
    'text' => "Administrator has not allowed any language yet!."
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}


if(!$ajax) {
?>
<div id="main-content">
<?php
}
if($ajax && isset($cssInc)) {
  foreach($cssInc as $c) {
    echo "<link rel=\"stylesheet\" href=\"css/$c\" />\n";
  }
}
?>


<div class="container">
  <div class="row">
   <div class="col-md-9 col-md-offset-2 col-xs-12"> 
      <div class="page-header">
        <h1>Upload Subtitles <small>for your video: "<?php echo $_GET["title"]?>"</small></h1>
      </div>
	 
	
      <form  id="upload-form" enctype="multipart/form-data" action="receiveSubtitle.php" method="post" role="form">        
		  <input type="hidden" name="MAX_FILE_SIZE" value="3000000" />	
		  	  
		   <div class="col-md-3 col-md-offset-1 ">		   	
				<label for="languageCode">Select Language:</label>
					<select class="form-control" id="languageCode" name="languageCode">
				
					<?php
					foreach($results as $result) { ?>
						$usr['id']
						<option value="<?php echo $result['id'];?>"> <?php echo $result['language'];?></option>
				
					<?php
					} 
					?>
				</select>
		
			</div>
			
			<input type="hidden" id="videoID" name="videoID" value="<?php echo$_GET["id"]?>">
		
			<div class="col-md-6 ">
			  
				<label for="input-id" class="control-label">Select File:</label>
                 <div id="errorBlock" class="help-block file-error-message" style="display: none;"></div>
				<input id="input-id" name="file" type="file" class="file">
				 
			</div>
		
			<!--<div class="col-md-2">
				<button type="submit" class="btn btn-primary">Upload</button>
			</div>-->
			
	 </form>
               
      </div>     
        <div class="col-md-6 col-md-offset-3 col-xs-12"> 
      	  <div class="row">
      		 <div class="row">
        	   <div class="progress">
        		<div id="progress" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">
         		 0%
        		</div>
			</div>       
          </div>
        </div>
       </div>    
    </div>
</div>

<div class="container">
 	<div class="row">
 		<div class="col-md-6 col-md-offset-3 col-xs-12"> 

			<div id="uploadSuccess" class="alert alert-success" style="display: none;" role="alert"><strong>Congratulations!</strong> File uploaded!</div>	  
      <div id="uploadFail" class="alert alert-danger" style="display: none;" role="alert"><strong>Sorry!</strong> <span id="failtext"></span></div>


		</div>
	</div>
</div>

<?php
//$jsInc = array("fileinput.min.js"); // Ekstra .js filer å inkludere.


$jsIncInline = array('
  initUploadSubtitles();
');
require_once 'include/footer.php';