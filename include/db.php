<?php
$debug = false;
$dbname = 'skole_wwwprosjekt';
$dbuser = 'root';
$dbpass = '';
$dbhost = '127.0.0.1';

try {	// Attempt a connection to the database
	$db = new PDO('mysql:host='.$dbhost.';dbname='.$dbname,$dbuser,$dbpass);
} catch (PDOException $e) {	// If an error is detected
  if(!(isset($install) && $install)) {
    if (isset($debug) && $debug)		// If we are doing development
      die('Unable to connect to database : '.$e->getMessage());
    else 					// Do NOT show above information to end users.
      die('Unable to connect to database, please try again later. maybe you need to <a href="install.php">install</a>');
  }
}
