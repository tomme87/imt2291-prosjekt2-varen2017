<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />
      
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" 
      integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />
    
    
    <!-- CSS for video-js and transcript -->
    <link rel="stylesheet" href="css/video-js.min.css" />
    <link rel="stylesheet" href="css/videojs-transcript.css" />
    <link rel="stylesheet" href="css/videojs.markers.min.css" />
    <link rel="stylesheet" href="css/fileinput.min.css" />
    
    <!-- consider making 1 css -->
    <link rel="stylesheet" href="css/thumbslider.css" />
    <link rel="stylesheet" href="css/player.css" />
    <link rel="stylesheet" href="css/search.css" />
    <link rel="stylesheet" href="css/main.css" />
     
<?php
if(isset($cssInc)) {
  foreach($cssInc as $c) {
    echo "<link rel=\"stylesheet\" href=\"css/$c\" />\n";
  }
}
?>

  </head>
<body>
  <nav class="navbar navbar-default">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a data-pjax class="navbar-brand" href="index.php">Video DB</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <form id="search-form" class="navbar-form navbar-left" action="search.php" method="GET" role="search">
            <div class="input-group">
              <div  class="form-group">
                <input type="text" name="search" class="form-control" placeholder="Search"  value="<?php if(isset($_POST['search'])) echo htmlspecialchars($_POST['search']); ?>">
              </div>
              <span class="input-group-btn">
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
              </span>
            </div>
          </form>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" id="dHM" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></a>
            <ul class="dropdown-menu" aria-labelledby="dHM">
              <!--<li><a href="#">Action</a></li>-->
              <?php
                if($user->isAdmin()) {
                  echo '<li class="dropdown-header">Admin tools</li>';
                  echo '<li><a data-pjax href="users.php">User list</a></li>';
                  echo '<li><a data-pjax href="adminLanguages.php">Admin Languages</a></li>';             				
                  echo '<li role="separator" class="divider"></li>';
                }
			
                if ($user->isLoggedIn()) { // Er logget inn
                  echo '<li><a data-pjax href="upload.php">New video</a></li>';                                   
                  echo '<li role="separator" class="divider"></li>';
                  echo '<li><a href="index.php?logout=true">Log out</a></li>';
                } else {
                  echo '<li><a data-pjax href="index.php">Front page</a></li>';
                }
              ?>
            </ul>
          </li>
        </ul>
        <div class="nav navbar-nav navbar-right">
          
          <?php 
            if ($user->isLoggedIn()) // Vis "logget inn som .." tekst
              echo '<p class="navbar-text">Signed in as <a href="#" class="navbar-link">'.$user->username.'</a></p>';
              //echo  '<a href="index.php?logout=true" class="btn btn-default navbar-btn">Logg ut</a>'; // Legg denne i dropdown i stede?
            else
              echo '<a data-pjax href="login.php" class="btn btn-default navbar-btn">Log in</a>';
          ?>
        </div>
        
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <div class="loader" style="display: none;"></div>