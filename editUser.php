<?php
/**
 *
 * editUser.js
 * 
 * Processes requests (from users.php) to change permissions for users.
 * Converts the users into teachers or students.
 *
 *  Se also: 
 * 		users.js
 * 
 */


session_start();          				 // Start the session
require_once 'include/db.php';			 // Connect to the database
require_once 'classes/user.inc.php'; 	 // Get user class


$user = new User($db); // Create user object.


	
echo $_POST['teacher'];
echo $_POST['id'];
if($_POST['teacher']==0){
	
	$user->makeStudent($_POST['id']);	
	
}else{
	
	$user->makeTeacher($_POST['id']);	
	
}