<?php
/**
 *
 * install.php
 *
 * Install the database 
 *
 */

$install = true;
require_once 'include/db.php'; // Connect to the database

if(isset($db)) die("Du har allerede installert");

if (isset($_POST["adm_usr"])) {
  try {	// Attempt a connection to the database
    $db = new PDO('mysql:host='.$dbhost.';',$_POST['adm_usr'],$_POST['adm_pass']);
    if (isset($debug) && $debug)
     $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {	// If an error is detected
    if (isset($debug) && $debug)		// If we are doing development
      die ('Unable to connect to database : '.$e->getMessage());
    else 					// Do NOT show above information to end users.
      die ('Unable to connect to database, please try again later');
  }

  $sqls = array(
    'FLUSH PRIVILEGES',
  );

  // Create the user.
  $sql = 'CREATE USER :dbuser@:dbhost IDENTIFIED BY :dbpass';
  $sth = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
  $sth->execute(array(
    ':dbuser' => $dbuser,
    ':dbhost' => $dbhost,
    ':dbpass' => $dbpass,
  ));

  // Give user user access to system
  $sql = 'GRANT USAGE ON *.* TO :dbuser@:dbhost REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0';
  $sth = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
  $sth->execute(array(
    ':dbuser' => $dbuser,
    ':dbhost' => $dbhost,
  ));

  // Create database
  $db->exec("CREATE DATABASE IF NOT EXISTS `$dbname`");


  // Give user access to database
  $sql = "GRANT ALL PRIVILEGES ON $dbname.* TO :dbuser@:dbhost";
  $sth = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
  $sth->execute(array(
    ':dbuser' => $dbuser,
    ':dbhost' => $dbhost,
  ));

  $db->exec('FLUSH PRIVILEGES'); // Flush

  // Use db.
  $db->exec("use $dbname");

  // Add user table
  $sql = "CREATE TABLE `users` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `email` varchar(255) COLLATE utf8_bin NOT NULL,
    `password` char(255) COLLATE utf8_bin NOT NULL,
    `username` varchar(30) COLLATE utf8_bin NOT NULL,
    `givenname` varchar(30) COLLATE utf8_bin NOT NULL,
    `surname` varchar(30) COLLATE utf8_bin NOT NULL,
    `admin` tinyint(1) NOT NULL DEFAULT '0',
    `teacher` tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY `email` (`email`),
    UNIQUE KEY `username` (`username`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
  $db->exec($sql);

  // Add persistent_login table
  $sql = "CREATE TABLE `persistent_login` (
    `uid` bigint(20) NOT NULL,
    `series` char(32) COLLATE utf8_bin NOT NULL,
    `token` char(255) COLLATE utf8_bin NOT NULL,
    `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`uid`,`series`),
    CONSTRAINT `persistent_login_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
  $db->exec($sql);

  // Add videos table
  $sql = "CREATE TABLE `videos` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `filename` varchar(255) COLLATE utf8_bin NOT NULL,
    `title` varchar(30) COLLATE utf8_bin NOT NULL,
    `description` text COLLATE utf8_bin NOT NULL,
    `userID` bigint(20) NOT NULL,
    `thumb` mediumblob NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `filename` (`filename`),
    KEY `userID` (`userID`),
    CONSTRAINT `videos_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
  $db->exec($sql);

  // Add playlist table
  $sql = "CREATE TABLE `playlist` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `userID` bigint(20) NOT NULL,
    `title` varchar(128) COLLATE utf8_bin NOT NULL,
    PRIMARY KEY (`id`),
    KEY `userID` (`userID`),
    CONSTRAINT `playlist_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
  $db->exec($sql);

  // Add playlists_videos table
  $sql = "CREATE TABLE `playlist_videos` (
    `playlistID` int(10) NOT NULL,
    `videoID` int(10) NOT NULL,
    `position` int(10) NOT NULL,
    PRIMARY KEY (`playlistID`,`videoID`),
    KEY `videoID` (`videoID`),
    CONSTRAINT `playlist_videos_ibfk_1` FOREIGN KEY (`playlistID`) REFERENCES `playlist` (`id`),
    CONSTRAINT `playlist_videos_ibfk_2` FOREIGN KEY (`videoID`) REFERENCES `videos` (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
  $db->exec($sql);

  // Add subtitles table
  $sql = "CREATE TABLE `subtitles` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `idVideo` int(10) NOT NULL,
    `idLanguage` char(2) COLLATE utf8_bin NOT NULL,
    `filename` varchar(255) COLLATE utf8_bin NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `filename` (`filename`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
  $db->exec($sql);
  
  // Create plays table
  $sql = "CREATE TABLE `plays` (
    `playID` bigint(30) NOT NULL AUTO_INCREMENT,
    `videoID` int(10) NOT NULL,
    `userID` bigint(20) NOT NULL,
    `played` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`playID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
  $db->exec($sql);
  
  // Create images table
  $sql = "CREATE TABLE `images` (
   `id` int(10) NOT NULL AUTO_INCREMENT,
   `videoID` int(10) NOT NULL,
   `startTime` decimal(10,4) NOT NULL,
   `endTime` decimal(10,4) NOT NULL,
   `file` varchar(255) COLLATE utf8_bin NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `file` (`file`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
  $db->exec($sql);


  // Add languages table
  $sql = "CREATE TABLE `languages` (
    `id` char(2) COLLATE utf8_bin NOT NULL,
    `language` varchar(20) COLLATE utf8_bin NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `language` (`language`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
  $db->exec($sql);
  
  // Insert default languages
  $sql = "INSERT INTO languages (id, language) VALUES 
            ('no', 'Norwegian'),
            ('en', 'English'),
            ('sv', 'Swedish'),
            ('da', 'Danish'),
            ('es', 'Spanish'),
            ('fr', 'French'),
            ('fi', 'Finnish'),
            ('de', 'German')";
  $db->exec($sql);
  
  // We should be able to connect now.
  try {	// Attempt a connection to the database
    $db = new PDO('mysql:host='.$dbhost.';dbname='.$dbname,$dbuser,$dbpass);
  } catch (PDOException $e) {	// If an error is detected
    die("Det oppsto en feil under oppretting av database.");
  }

  echo "Databasen ble installert. Gå til <a href='login.php'>Login siden</a> og registerer en bruker. Den første brukeren vil få administratortilgang.";
  exit;

}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Installer VideoDB</title>
  </head>
  <body>
    <h1>Installer VideoDB</h1>
    <ol>
      <li>Sørg for at webserver har skrive rettigheter til uploads, transcripts og temp mappe.</li>
      <li>Legg ffprobe og ffmpeg inn i bin katalogen (til dette prosjektet) - Les README i bin katalogen for nærmere informasjon.</li>
      <li>Rediger include/db.php med ønsket brukernavn og passord osv.</li>
      <li>Fyll inn formen nedenfor og oprette database</li>
    </ol>
    <form action="install.php" method="POST">
      <label for="adm_usr">Admin bruker: </label><input type="text" name="adm_usr" value="root"><br />
      <label for="adm_pass">Admin passord: </label><input type="password" name="adm_pass"><br />
      <input type="submit" value="Opprett">
    </form>
  </body>
</html>