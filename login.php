<?php
/**
 * login.php
 * 
 * Shows a form where the user can  Sign in or register
 * 
 * HTML/CSS/JS Hentet fra http://bootsnipp.com/snippets/featured/login-and-register-tabbed-form
 * Deretter modifisert for v�rt bruk
 *
 */
session_start();					// Start the session
require_once 'include/db.php'; // Connect to the database
require_once 'classes/user.inc.php'; // Get user class

$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) { // request was sent with ajax, show only content.
  $ajax = true;
}

$user = new User($db); // Create user object.

$pageTitle = 'Video DB - Registrer deg'; // Finn p� no :-P
$cssInc = array("login-register.css"); // Ekstra .css filer � inkludere.
if(!$ajax) // Don't create header on ajax request.
  require_once 'include/header.php';



$newUserError = false;
if (isset($_POST['givenname'])) {	// Create new user
  if(isset($_POST['password']) && isset($_POST['confirm-password']) 
      && $_POST['password'] == $_POST['confirm-password']) // Sjekker at password og confirm-password er like.
    $res = $user->addUser($_POST['username'], $_POST['password'], $_POST['givenname'], $_POST['surname'], $_POST['emailr']);
  else { // De er ikke like.
    $user->unknownUser = 'Passwords do not match.';
    $newUserError = true;
  }
	if (isset($res['success'])) // Send til index om bruker registrerte seg.
    header("Location: index.php");
  else
    $newUserError = true;
}




if(!$ajax) {
?>
<div id="main-content">
<?php
}
if($ajax && isset($cssInc)) {
  foreach($cssInc as $c) {
    echo "<link rel=\"stylesheet\" href=\"css/$c\" />\n";
  }
}
?>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-6">
                <a href="#" class="active" id="login-form-link">Login</a>
              </div>
              <div class="col-xs-6">
                <a href="#" id="register-form-link">Register</a>
              </div>
            </div>
            <hr>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">
                <?php
                if($user->unknownUser != null)
                  echo '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> '.$user->unknownUser.'</div>';
                ?>
                <form data-pjax id="login-form" action="login.php" method="post" role="form" style="display: block;">
                  <div class="form-group">
                    <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="<?php if(isset($_POST['email'])) echo htmlspecialchars($_POST['email']); ?>">
                  </div>
                  <div class="form-group">
                    <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
                  </div>
                  <div class="form-group text-center">
                    <input type="checkbox" tabindex="3" class="" name="remember" id="remember" value="1">
                    <label for="remember"> Remember Me</label>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6 col-sm-offset-3">
                        <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                      </div>
                    </div>
                  </div>
                  
                </form>
                <form data-pjax id="register-form" action="login.php" method="post" role="form" style="display: none;">
          
    
          
                  <div class="form-group">
                    <input type="text" name="givenname" id="givenname" tabindex="1" class="form-control" placeholder="First Name" value="<?php if(isset($_POST['givenname'])) echo htmlspecialchars($_POST['givenname']); ?>">
                  </div>
          
                  <div class="form-group">
                    <input type="text" name="surname" id="surname" tabindex="1" class="form-control" placeholder="Surname" value="<?php if(isset($_POST['surname'])) echo htmlspecialchars($_POST['surname']); ?>">
                  </div>
       
          
                  <div class="form-group">
                    <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="<?php if(isset($_POST['username'])) echo htmlspecialchars($_POST['username']); ?>">
                  </div>
                  <div class="form-group">
                    <input type="email" name="emailr" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="<?php if(isset($_POST['emailr'])) echo htmlspecialchars($_POST['emailr']); ?>">
                  </div>
                  <div class="form-group">
                    <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password">
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6 col-sm-offset-3">
                        <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- </div> (Skip last end div, this is in footer.php -->

<?php
if($newUserError) { // Show Register page.
  $jsIncInline = array( // Add inline JS
    '$(function() {
      $("#login-form").hide();
      $("#register-form").show();
      $("#login-form-link").removeClass("active");
      $("#register-form-link").addClass("active");
    });'
  );
}
$jsIncInline = array('
  initFormActions();
');
require_once('include/footer.php');