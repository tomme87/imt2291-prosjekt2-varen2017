<?php
/**
 * Users.php
 *
 * Displays a table with informationo about users:
 * ID, email, username, etc.
 *
 * Each row has two buttons:
 *
 * 1. A button to change the user rights (Teacher, Student)
 * 2. Another button to delete a user.
 *
 *
 * See also:
 * EditUser.php
 * Users.js
 * 
 * Sources:
 *    datatables: https://datatables.net ( https://github.com/DataTables/DataTables )
 */
session_start();          // Start the session
require_once 'include/db.php'; // Connect to the database
require_once 'classes/user.inc.php'; // Get user class

$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) { // request was sent with ajax, show only content.
	$ajax = true;
}

$user = new User($db); // Create user object.

$pageTitle = 'Video DB - User list'; // Finn p� no :-P
//$cssInc = array("jquery.dataTables.css", "dataTables.bootstrap.min.css"); // Ekstra .css filer � inkludere.
//require_once 'include/header.php';







if(isset($_POST['delete']) && $user->isAdmin()) { // Attempting to delete
  $res = $user->delete($_POST['delete']);
  if(isset($res['success'])) { // delte success. 
  	echo json_encode(array('success' => 'success', 'description' => 'The user was deleted'));
  } else { // delte failed send sort message.
  	echo json_encode(array('error' => 'error', 'description' => $res['description']));
  }
  exit;
}

if(!$ajax) // No header on ajax requests.
	require_once 'include/header.php';

if(!$user->isAdmin()) { // User is NOT admin.
  $msg = array(
    'title' => "Sorry!",
    'text' => "You have to be admin to view userlist!."
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}



// Find users.
$users = $user->find(isset($_GET['search']) && $_GET['search'] ? $_GET['search'] : '_');

if(!$ajax) {
?>
<div id="main-content">
<?php
}
if($ajax && isset($cssInc)) {
  foreach($cssInc as $c) {
    echo "<link rel=\"stylesheet\" href=\"css/$c\" />\n";
  }
}
?>
<div class="container">
  <!--<div class="row">
    <div class="col-md-8 col-md-offset-2 col-xs-12">
      <form action="users.php" method="GET">
      <div class="input-group">
        <input name="search" type="text" class="form-control" placeholder="Search for username..." value="<?php if(isset($_GET['search'])) echo $_GET['search']; ?>">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">Go!</button>
        </span>
      </div>
      </form>
    </div>
  </div>-->
  
  <div class="row" style="margin-top:20px;">
    <div class="col-md-12 col-xs-12">
    <?php
      if(isset($_GET['deleted']))
        echo '<div id="uploadSuccess" class="alert alert-success" role="alert"><strong>Success!</strong> User deleted!</div>';
      
    if(count($users)) {
    ?>
      <table id="user-table" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>id</th>
            <th>email</th>
            <th>username</th>
            <th>givenname</th>
            <th>surname</th>
            <th>teacher</th>
     
            <th>admin</th>
            <th>delete</th>
          </tr>
        </thead>
        <tbody>
        <?php
        foreach($users as $usr) {
          echo '<tr>';
          echo '<td>'.$usr['id']."</td>\n";
          echo '<td>'.$usr['email']."</td>\n";
          echo '<td>'.$usr['username']."</td>\n";
          echo '<td>'.$usr['givenname']."</td>\n";
          echo '<td>'.$usr['surname']."</td>\n";
    
          ?>  
    

                              
        	<td>        
      	  		<div class="dropdown">
 			 		 <button   id="MyButton<?php echo $usr['id']; ?>" class="btn btn-sm btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" > <?php echo ($usr['teacher'] == 1 ? 'Yes' : 'No') ?>
     				 <span class="caret"></span>
  					 </button>
    					<ul id="MyDropdown" class="dropdown-menu"  value="Fast" >
    					 <li><a  data-id="<?php echo $usr['id']; ?>" data-value="1">Yes</a></li>
    					 <li><a  data-id="<?php echo $usr['id']; ?>" data-value="0">No</a></li>	 				 	
     				 	   
  					</ul>     				 
				</div>           
      		</td>
       
       
          <?php
          
          echo '<td>'.($usr['admin'] == 1 ? 'Yes' : 'No')."</td>\n";
        
        ?>
          <td>
            <form class="delete">
              <button type="submit" name="delete" value="<?php echo $usr['id']; ?>"  class="btn btn-default btn-xs"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></button>
            </form>
          </td>
          </tr>
        <?php
        }
        ?>
        </tbody>
      </table>
      <?php 
      } else {
        echo '<div style="margin-top:20px;" class="alert alert-danger" role="alert"><strong>Sorry!</strong> Ingen brukere funnet.</div>';
      }
      ?>
    </div>
  </div>
  
</div>



<?php
//$jsInc = array("jquery.dataTables.js", "dataTables.bootstrap.min.js", "users.js"); // Ekstra .js filer � inkludere.



$jsIncInline = array('
initUsers(); $("#user-table").DataTable();
');


require_once 'include/footer.php';