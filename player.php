<?php
/**
 * player.php:
 * 
 * Here users can:
 * 		Watch a video:
 * 		View statistics of a video (If user owns the video)
 * 		View, edit and add subtitles
 * 		Create and edit playlist  
 *      
 *    
 * Sources:
 *  videojs: http://videojs.com/
 *  videojs-transcript: https://github.com/walsh9/videojs-transcript
 * 
 */
session_start();					// Start the session
require_once 'include/db.php'; // Connect to the database
require_once 'classes/user.inc.php'; // Get user class
require_once 'classes/video.inc.php'; // Get video class
require_once 'classes/playlist.inc.php'; // Get playlist class
require_once 'classes/subtitle.inc.php'; // Get subtitle class

$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) { // request was sent with ajax, show only content.
  $ajax = true;
}

$user = new User($db); // Create user object.

$pageTitle = 'Video DB - Player'; // Finn på no :-P
//$cssInc = array("video-js.min.css", "videojs-transcript.css"); // Ekstra .css filer å inkludere.
if(!$ajax) // No header on ajax requests.
  require_once 'include/header.php';

if(!isset($_GET["id"]) && !isset($_GET['playlist'])){
  $msg = array(
    'title' => "Sorry!",
    'text' => "Missing video/playlist ID!"
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

if(!isset($_GET['id']) && isset($_GET['playlist'])) { // Playlist without videoID given. Fetching first video.
  $playlist = new Playlist($db, $_GET['playlist']);
  $video = $playlist->getVideos()[0]['video']; // Create video object
} else {
  $video = new Video($db, $_GET["id"]); // Create video object
}

if($video->error) {
  $msg = array(
    'title' => "Sorry!",
    'text' => $video->error
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

if(isset($_GET['playlist']) && $_GET['playlist'] && !isset($playlist)) {
  $playlist = new Playlist($db, $_GET['playlist']);
  // Find out if this video is in this playlist.
} elseif(!isset($playlist)) {
  $playlist = new Playlist($db);
}

if(!$ajax) {
?>
<div id="main-content">
<?php
}
if($ajax && isset($cssInc)) {
  foreach($cssInc as $c) {
    echo "<link rel=\"stylesheet\" href=\"css/$c\" />\n";
  }
}
?>

<div class="container">
  <div class="page-header">
    <h1><span id="info-title"><?php echo $video->getTitle();?></span><?php if($playlist->isCreated()) echo ' <small>Playlist: '.htmlspecialchars($playlist->title).'</small>'; ?></h1>
  </div>
  <div class="row">
    <div class="col-md-8">
      <div class="pull-left">
        <p>Uploaded by: <strong id="info-by"><?php echo $video->getUsername(); ?></strong> 
                	
   			<?php if( ($user->isLoggedIn() && ($user->getUID() == $video->getOwnerID()) && $user->isTeacher()) || ($user->isLoggedIn() && $user->isAdmin()))  {  ?>
   			
   			 
   			 	 <button class="btn btn-primary" data-toggle="modal" data-target="#statisticsModal" type="button">
  					Views: <span id="statisticsSpan" class="badge"><?php echo $video->getNumberOfViews(); ?></span>
				</button>
   			    			   	 
   			<?php } else {?>  |  Views: <span id="statisticsSpan" class="badge"><?php echo $video->getNumberOfViews(); ?> </span> <?php }	?>
         	
   				         
        </p>
         
      </div>
      <div class="pull-right">
        <?php
        if($user->isLoggedIn()) { // Show toolbars if user logged in.
        ?>
        <div class="btn-toolbar" role="toolbar">
          <?php
          if($video->getOwnerID() == $user->getUID()) {
          ?>
          <div class="btn-group" role="group">
            <a data-pjax href="subtitleEditor.php?id=<?php echo $video->id; ?>" class="btn btn-default">Edit subtitle</a>
          </div>
          <?php
          }
          ?>
          <div class="btn-group" role="group">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              Add to playlist
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
            <?php
            $found = false;
            $myPlaylists = $playlist->getPlaylistsByUser($user->uid); // Get playlist created by current user.
            foreach($myPlaylists as $pl) {
              if(!$pl->isInPlaylist($video->id)) { // This video is not this playlist.
                echo '<li><a class="addVideo" href="takeEdit.php?do=addVideo&id='.$pl->id.'&vid='.$video->id.'">'.$pl->title.'</a></li>'; // Should change this to form+POST instead.
                $found = true;
              }
            }
            if($found) // A playlist was found, so we need a seperator.
              echo '<li role="separator" class="divider"></li>';
            ?>
              <li><a href="#" data-toggle="modal" data-target="#myModal">New playlist</a></li>
            </ul>
          </div>
          <?php
          if($video->userid == $user->uid || $user->isAdmin()) { // If owner of this video or admin.
          ?>
          <div class="btn-group" role="group">
                <a data-pjax href="uploadSubtitles.php?id=<?php echo $video->id; ?>&title=<?php echo $video->title;?>" class="btn btn-default">Add transcript</a>
           <!-- <a href="uploadSubtitles.php?id=<?php echo $video->id; ?>&title=<?php echo $video->title;?>" class="btn btn-default">Add transcript</a> -->
          </div>
          <div class="btn-group" role="group">
            <a class="btn btn-default" href="#" data-toggle="modal" data-target="#myModal2">Edit</a>
          </div>
          <!--<div class="btn-group" role="group">
            <a href="uploadSubtitles.php?id=<?php echo $video->id; ?>&title=<?php echo $video->title;?>" class="btn btn-default">Delete video</a>
          </div>-->
          <?php
          }
          ?>
        </div>
        <?php
        }
        ?>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-8">
      <div id="videoHolder">
        <div  class="embed-responsive embed-responsive-16by9">
          <video 
            id="my-player"           
            class="video-js embed-responsive-item"
            controls
            preload="auto"
            poster="<?php echo $video->getThumb(); ?>"
            data-videoID="<?php echo $video->id; ?>"
            data-userID="<?php echo $user->uid; ?>"
          >
            <source src= <?php echo $video->getPath(); ?> type="video/mp4">
        
        <?php
         $results = array();
         $subtitle=new Subtitle($db);
         $results = $subtitle->findSubtitles($video->id); //Prøver å finne subtitles for videoen
        
         foreach($results as $result) {?> 		
        <?php $idLang=$result->idLanguage;?>
        <track class="subtitle" kind="captions" src="<?php echo $result->getPath();?>" srclang="<?php echo $result->idLanguage;?>" label="<?php echo $result->getLanguage();?>">
         <?php 	
         }
             ?> 
             <track default class="imgsub" kind="metadata" src="imageVTT.php?id=<?php echo $video->id; ?>" label="images">
            <p class="vjs-no-js">
              To view this video please enable JavaScript, and consider upgrading to a
              web browser that
              <a href="http://videojs.com/html5-video-support/" target="_blank">
                supports HTML5 video
              </a>
            </p>
          </video>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div id="transcript"></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-8">
      <div class="well">
      <!--<h3>Beskrivelse</h3>-->
      <?php
      // Show description
      echo '<p id="vidDescription">'.$video->description.'<p>';
      ?>
      </div>
    </div>
  </div>
  
  <?php
  if($playlist->isCreated()) { // Show playlist if created.
  ?>
  <div class="row">
    <div class="col-md-12">
      <h3>Playlist <?php if($user->isLoggedIn() && $playlist->userid == $user->uid) { ?><small><a data-pjax href="editPlaylist.php?id=<?php echo $playlist->id ?>">edit</a></small><?php } ?></h3>
      <div class="well">
            <?php
            $last = $playlist->getVideos();
            $total = count($last);
            if($total) {
            ?>
            <div id="myCarousel" class="carousel slide">
                
                <!-- Carousel items -->
                <div class="carousel-inner">
                <?php
                $count = 0;
                foreach($last as $vida) {
                  $vid = $vida['video'];
                  if(($count++ % 4) == 0) {
                    ?>
                    <div class="item">
                      <div class="row">
                    <?php
                  }
                  ?>
                        <div class="col-sm-3">
                          <div class="thumbnail<?php if($vid->id == $video->id) echo ' current'; ?>">
                            <a data-pjax href="player.php?id=<?php echo $vid->id.'&playlist='.$playlist->id; ?>" data-id="<?php echo $vid->id ?>" class="thumbnail playlistVideo">
                              <img src="<?php echo $vid->getThumb(); ?>" alt="Image" class="img-responsive">
                              <div class="carousel-caption">
                                <h3><?php echo $vid->getTitle(25); ?></h3>
                                <p><?php echo $vid->getDescription(25); ?></p>
                              </div>
                            </a>
                          </div>
                        </div> 
                  <?php
                  if(($count % 4) == 0 || $count == $total) {
                    ?>
                      </div>
                    </div>
                    <?php
                  }
                }
                ?>
                </div>
                <!--/carousel-inner--> <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>

                <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
            </div>
            <!--/myCarousel-->
            <?php
            } else {
              echo '<p>Ingen videoer.</p>';
            }
            ?>
        </div>
        <!--/well-->
    </div>
  </div>
  <?php
  }
  ?>
</div>

<img style="display: none;" class="largeImage" src='https://dl.dropboxusercontent.com/s/zn76w68cnu94682/CUsersTommeAppDataRoamingNotepad%2B%2Bpluginsc_2017-05-02_20-38-19.png' />

<?php
// Modal for create playlist.
if($user->isLoggedIn()) { // Don't load modal if user not logged in.
?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Create new playlist</h4>
      </div>
      <form id="newPlaylist" method="POST" action="takeEdit.php?do=newPlaylist">
        <input type="hidden" name="id" value="<?php echo $video->id; ?>">
        <div class="modal-body">
          <div class="form-group">
            <label for="newPlaylistName">Enter name of new playlist</label>
            <input name="plName" type="text" class="form-control" id="newPlaylistName" placeholder="Name">
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="addThis" value="1" checked> Add this video to playlist
            </label>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add playlist</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
// MODAL for edit video.
  if($video->userid == $user->uid || $user->isAdmin()) { // If owner of this video or admin.
?>
<!-- Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel2">Edit Video</h4>
      </div>
      <form id="editVideo" method="POST" action="takeEdit.php?do=editVideo">
        <input type="hidden" name="id" value="<?php echo $video->id; ?>">
        <div class="modal-body">
          <div class="form-group">
            <label for="videoName">Title</label>
            <input name="title" type="text" class="form-control" id="videoName" placeholder="Title" value="<?php echo $video->title ?>">
          </div>
          <div class="form-group">
            <label for="videoDescription">Description</label>
            <textarea name="description" class="form-control" id="videoDescription"><?php echo htmlspecialchars($video->description) ?></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Edit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
  }
}

// MODAL for statistics
// Don't load modal if user is not the teacher owner of the video or ADMIn

if( ($user->isLoggedIn() && ($user->getUID() == $video->getOwnerID()) && $user->isTeacher()) || ($user->isLoggedIn() && $user->isAdmin()))  {  ?>
<!-- Modal -->
<div class="modal fade" id="statisticsModal" tabindex="-1" role="dialog" aria-labelledby="statisticsModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Usage statistics of the video: <strong>"<?php echo $video->getTitle(); ?>" </strong> </h4>
         <p>Uploaded by: <strong id="info-by"><?php echo $video->getUsername(); ?></strong>  |  Viewed: <span class="badge"><?php echo $video->getNumberOfViews(); ?> </span> times </p>
        
       
      </div>
    
        <div class="modal-body">
         <?php
         $views=$video->getDetailsOfViews();
         
         if(count($views)) {
         ?>
          <table id="statistics-table" class="table table-striped table-bordered">
          <thead>
            <tr>
                                               
              <th>UserID</th>
              <th>Username</th>
              <th>Name</th>
              <th>Surname</th>
              <th>No.of Views</th>
          
            </tr>
          </thead>
          <tbody>
      		  <?php
        	  foreach($views as $vws) {
        	  	       	  	
        	  	
        	  	if($vws['userID'] <0){
        	  		
        	  		echo '<tr>';
        	  		echo '<td>' ."Anonymous". "</td>\n";
        	  		echo '<td>' ."Anonymous". "</td>\n";
        	  		echo '<td>' ."Anonymous". "</td>\n";
        	  		echo '<td>' ."Anonymous". "</td>\n";
        	  		echo '<td>'.$vws['views']. "</td>\n";
        	  		echo '</tr>';
        	  		
        	  	}else{
        	  		echo '<tr>';
         	 		echo '<td>'.$vws['userID']."</td>\n";
         	  		echo '<td>'.$vws['username']."</td>\n";
           	 		echo '<td>'.$vws['givenname']."</td>\n";
         	 		echo '<td>'.$vws['surname']."</td>\n";
       		 		echo '<td>'.$vws['views']."</td>\n";
       		  		echo '</tr>';
        	  	}	
              }
          
            ?>
            
            </tr>
          </tbody>          
        </table>
        <?php
          }
         ?>
      </div>      
        
       <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>   
      
    </div>
  </div>
</div>
<?php
}
?>

<?php
$jsIncInline = array('
  initPlayer(); $("#statistics-table").DataTable();
');
require_once 'include/footer.php';