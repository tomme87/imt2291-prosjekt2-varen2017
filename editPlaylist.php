<?php
/**
 * editPlaylist.php
 * 
 * Lists all videos in playlist so that you can rearrange them.
 * 
 * Check also: 
 * 	editPlaylist.js
 */
session_start();					// Start the session
require_once 'include/db.php'; // Connect to the database
require_once 'classes/user.inc.php'; // Get user class
require_once 'classes/video.inc.php'; // Get video class
require_once 'classes/playlist.inc.php'; // Get playlist class

$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) { // request was sent with ajax, show only content.
	$ajax = true;
}

$user = new User($db); // Create user object

$pageTitle = 'Video DB - Edit Playlist'; // Finn på no :-P
if(!$ajax) // No header on ajax requests.
  require_once 'include/header.php';

if(!isset($_GET['id'])) {
  $msg = array(
    'title' => "Sorry!",
    'text' => "Missing playlist ID!"
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

$playlist = new Playlist($db, $_GET['id']);

if($playlist->error) {
  $msg = array(
    'title' => "Sorry!",
    'text' => $playlist->error
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

if($playlist->userid != $user->uid) { // kun eier kan redigere.
  $msg = array(
    'title' => "Sorry!",
    'text' => "Du eier ikke denne spillelisten"
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

$videos = $playlist->getVideos();

if(!count($videos)) {
  $msg = array(
    'title' => "Sorry!",
    'text' => "Ingen videoer i denne spillelisten."
  );
	require_once ('include/showMsg.php'); // Vis kort melding.
  exit;
}

if(!$ajax) {
?>
<div id="main-content">
<?php
}

?>



<div class="container">
  <div class="page-header">
    <h1>Edit playlist <small><a data-pjax href="player.php?playlist=<?php echo $playlist->id; ?>">View</a></small></h1>
  </div>
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading"><?php echo htmlspecialchars($playlist->title) ?></div>
    <div class="panel-body">
      <p>Drag the video in position and click save.</p>
    </div>

    <!-- List group -->
    <ul class="list-group sortable">
      <?php
      foreach($videos as $video) {
        echo '<li class="list-group-item video-item" draggable="true" data-vid="'.$video['video']->id.'" data-position="'.$video['position'].'">'
                .'<button class="btn btn-default delvid" data-id="'.$playlist->id.'" data-loading-text="Deleteing...">delete</button> '.$video['video']->title.'</li>';
      }
      ?>
    </ul>
  </div>
  <button id="saveButton" class="btn btn-primary" data-id="<?php echo $playlist->id; ?>" data-loading-text="Saving...">Save</button>
  <div id="alertbox" class="alert alert-success alert-dismissible" style="display: none; margin-top: 10px;" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Playlist saved.
  </div>
</div>

<?php

$jsIncInline = array('
  initEditPlaylist();
');
require_once 'include/footer.php';