<?php
/**
 * ReceiveFile.php:
 *
 *  A video is stored in Db and disk
 *
 *
 */


session_start();							// Start the session
header ("Content-type: application/json");	// Vi sender svaret som json data

require_once 'include/db.php';				// Connect to the database
require_once 'classes/user.inc.php'; 		// Get user class
$user = new User($db); // Create user object.

require_once 'classes/video.inc.php'; // Get Video class.

$fn = $_FILES['file']['name'];
if (isset($_FILES['file']['name'])) {									// Dersom en fil er mottatt
  $fn = $_FILES['file']['name']; // Filnanvn.
  /* Burde ta en ekstra sjekk om $fn er video her ? evt i addVideo()..? (Det gjøres også i JS) */
  $video = new Video($db); // Video object.
  $res = $video->addVideo($fn, $_POST['title'], $_POST['description'], $user->getUID());
  if(isset($res['success'])) { // Successfull add.
    echo json_encode(array('ok'=>'OK', 'video' => get_object_vars($video)));
  } else {
    echo json_encode($res);
	
  }
} else {
  echo json_encode(array('error' => 'error', 'description' => 'Filen ble ikke motatt'));
}


