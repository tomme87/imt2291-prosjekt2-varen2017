<?php

/**
 * overview.php
 * 
 * Shows statistics of a particular video.
 * The user must be a teacher and the owner of the video
 * 
 *  Se also:
 * 	  player.php	
 *
 */



session_start();					// Start the session
require_once 'include/db.php'; // Connect to the database
require_once 'classes/user.inc.php'; // Get user class
require_once 'classes/video.inc.php'; // Get Video class.

$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) { // request was sent with ajax, show only content.
	$ajax = true;
}
$user = new User($db); // Create user object.

$pageTitle = 'Video DB - Statistics of usage'; // Finn på no :-P
if(!$ajax)
	require_once 'include/header.php';


if(!$user-> isTeacher()()) { // User is NOT admin.
	$msg = array(
			'title' => "Sorry!",
			'text' => "You have to be teacher to view the statistics of your videos!."
	);
	require_once ('include/showMsg.php'); // Vis kort melding.
	exit;
}

$video = new Video($db); // Video object.


?>



 <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>Statistics of usage</h3>
        
        <table id="statistics-table" class="table table-striped table-bordered">
          <thead>
            <tr>
           
              <th>ID Video</th>            
              <th>Thumbnail</th>
              <th>Tittel</th>               
              <th>Total Views</th>       
              <th>Total Users</th>
              <th>User</th>
              <th>Views</th>
              <th>Views Registry</th>
 
            </tr>
          </thead>
          <tbody>
          
            <?php
            //foreach($alla as $pl) {
             // echo '<tr class="clickable-row" data-href="player.php?playlist='.$pl->id.'"><td>'.$pl->id.'</td><td>'.$pl->title.'</td><td>'.$pl->lastVideoPos().'</td><td>'.$pl->userid.'</td></tr>';
            //}
            ?>
          </tbody>
        </table>
      </div>
    </div>
   </div>