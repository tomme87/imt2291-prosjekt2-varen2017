<?php
/**
 * This file handles requests to edit video/playlist
 */
session_start();					// Start the session
require_once 'include/db.php'; // Connect to the database
require_once 'classes/user.inc.php'; // Get user class
require_once 'classes/video.inc.php'; // Get video class
require_once 'classes/playlist.inc.php'; // Get playlist class
require_once 'classes/subtitle.inc.php'; // Get subtitle class

$user = new User($db); // Create user object.

if(!$user->isLoggedIn()) die; // Need to be logged inn.

if(!isset($_GET['do'])) die; // We should get something to do.
$do = $_GET['do'];

if($do == 'newPlaylist' && isset($_POST['id']) && isset($_POST['plName']) && $user->isLoggedIn()) { // Add new playlist
  $playlist = new Playlist($db);
  $playlist->create($_POST['plName'], $user->uid);
  if(isset($_POST['addThis']) && $_POST['addThis'] == 1) {
    $playlist->addVideo($_POST['id']);
  }
  //echo 'Location: player.php?id='.urlencode($_POST['id']).'&playlist='.$playlist->id;
  //header('Location: player.php?id='.urlencode($_POST['id']).'&playlist='.$playlist->id);
  echo json_encode(array('ok' => 'ok', 'url' => 'player.php?id='.urlencode($_POST['id']).'&playlist='.$playlist->id));
} elseif($do == 'editVideo' && isset($_POST['id']) && isset($_POST['title']) && $user->isLoggedIn()) { // Edit video
  $video = new Video($db, $_POST['id']);
  if(!$video->error && ($video->id == $user->uid || $user->isAdmin())) { // Owner or admin
    $video->edit($_POST['title'], $_POST['description']);
  }
  //echo 'Location: player.php?id='.urlencode($_POST['id']).'&playlist='.$playlist->id;
  //header('Location: player.php?id='.urlencode($_POST['id']).'&playlist='.$playlist->id);
  echo json_encode(array('ok' => 'ok', 'video' => get_object_vars($video)));
} elseif($do == 'addVideo' && isset($_GET['id']) && isset($_GET['vid'])) { // Add video to playlist
  $playlist = new Playlist($db, $_GET['id']);
  if(!$playlist->error && $playlist->userid == $user->uid) { // no error and is owner.
    $playlist->addVideo($_GET['vid']);
  }
  echo json_encode(array('ok' => 'ok', 'url' => 'player.php?id='.urlencode($_GET['vid']).'&playlist='.$playlist->id));
  //header('Location: player.php?id='.urlencode($_GET['vid']).'&playlist='.$playlist->id);
} elseif($do == 'editPlaylist' && isset($_POST['videos']) && isset($_GET['id'])) { // Edit playlist sorting
  $videos = $_POST['videos'];
  $playlist = new Playlist($db, $_GET['id']);
  if(!$playlist->error && $playlist->userid == $user->uid) { // no error and is owner.
    $playlist->updateVideoPosition($videos); // MANGLER MYE ERROR SJEKKING HER.
  }
  echo "done";
} elseif($do == 'delFromPlaylist' && isset($_POST['vid']) && isset($_GET['id'])) { // delete vid from playlist
  $playlist = new Playlist($db, $_GET['id']);
  if(!$playlist->error && $playlist->userid == $user->uid) { // no error and is owner.
    $playlist->removeVideo($_POST['vid']);
  }
  echo "done ".$_GET['id'].' '.$_POST['vid'];
} elseif($do == 'saveSub' && isset($_POST['vtt']) && isset($_POST['id']) && isset($_POST['vid'])) {
  $sub = new Subtitle($db, $_POST['id']);
  $video = new Video($db, $_POST['vid']);
  if($video->getOwnerID() == $user->getUID()) {
    file_put_contents($sub->getPath(), $_POST['vtt']);
    echo "done";
  } else {
    echo "error, permission denied";
  }
} elseif($do == 'delPic' && isset($_POST['id']) && isset($_POST['vid'])) {
  $video = new Video($db, $_POST['vid']);
  $res = $video->delPic($_POST['id']);
  if(!$res) echo 'error';
  else echo 'done';
}
