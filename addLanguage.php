<?php

/**
* addLanguage.php
*
* Treats request to add new languages
* 		 
*/





session_start();          		// Start the session
require_once 'include/db.php'; // Connect to the database
require_once 'classes/user.inc.php'; // Get user class
require_once 'classes/language.inc.php'; // Get language class
header("Content-type: application/json");



if (isset($_POST['languageID']) && isset($_POST['languageName'])){
		
		$language = new Language ($db, $_POST['languageID'],$_POST['languageName']);
				
				if($language->message==1){	              				  
				   
					//echo json_encode(array('success'=>'success'));
					echo json_encode(array('success' => 'success', 'description' => 'The language was saved'));
					
				}else {
				    								  
					echo json_encode(array('error' => 'error', 'description' => 'The language could not be saved'));
				}
}